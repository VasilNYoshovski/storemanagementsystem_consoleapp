﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services;
using StoreSystem.Services.Providers;
using StoreSystem.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Tests.Services.SaleServiceTests
{
    [TestClass]
    public class UpdateSaleDeliveryDate_Should
    {
        [TestMethod]
        public void ChangeDeliveryDate_WhenValidSaleIdAndDateArePassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int validSaleId;
            DateTime validDate;

            using (var getContext = new StoreSystemDbContext(options))
            {
                var sale = getContext.Sales.First();
                validSaleId = sale.SaleID;
                validDate = sale.OrderDate.AddDays(1);
            }

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var sut = new SaleService(context, dateTimeNowProvider.Object);

                //Act
                var isExecuted = sut.UpdateSaleDeliveryDate(validSaleId, validDate);

                //Assert
                Assert.AreEqual(validDate, context.Sales.Find(validSaleId).DeliveryDate);
            }
        }

        [TestMethod]
        public void ThrowsArgumentException_WhenInvalidSaleIdIsPassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int invalidSaleId = 100;

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                var errorText = string.Format(
                                       Consts.ObjectIDNotExist,
                                       nameof(Sale),
                                       invalidSaleId);
                //Act
                //Assert
                Assert.AreEqual(
                    errorText,
                    Assert.ThrowsException<ArgumentException>(() => sut.UpdateSaleDeliveryDate(invalidSaleId, new DateTime())).Message
                    );
            }
        }


        [TestMethod]
        public void ThrowsArgumentException_WhenInvalidDateIsPassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int validSaleId;
            DateTime invalidDate;

            using (var getContext = new StoreSystemDbContext(options))
            {
                var sale = getContext.Sales.First();
                validSaleId = sale.SaleID;
                invalidDate = sale.OrderDate.AddDays(-1);
            }

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                var errorText = string.Format(
                       Consts.DateError,
                       invalidDate,
                       invalidDate.AddDays(1));
                //Act
                //Assert
                Assert.AreEqual(
                    errorText,
                    Assert.ThrowsException<ArgumentException>(() => sut.UpdateSaleDeliveryDate(validSaleId, invalidDate)).Message
                    );
            }
        }
    }
}