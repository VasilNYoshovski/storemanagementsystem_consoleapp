﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services;
using StoreSystem.Services.Dto;
using StoreSystem.Services.Providers;
using StoreSystem.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Tests.Services.SaleServiceTests
{
    [TestClass]
    public class AddProductsToSale_Should
    {
        [TestMethod]
        public void AddProductsToSale_WhenValidParametersArePassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int validSaleID;

            var validProductName1 = "valid product 1";
            var validProductName2 = "valid product 2";

            using (var getContext = new StoreSystemDbContext(options))
            {
                validSaleID = getContext.Sales.Include(x => x.ProductsInSale).First(x => x.ProductsInSale.Count == 0).SaleID;
            }

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var validDate = new DateTime(2019, 4, 1);
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                ProductQuantityDto[] products = new[]
                {
                    new ProductQuantityDto(validProductName1,1),
                    new ProductQuantityDto(validProductName2,1),
                };

                //Act
                var isExecuted = sut.AddProductsToSale(validSaleID, products);

                //Assert
                Assert.IsTrue(isExecuted);
                Assert.AreEqual(2, context.Sales.Include(x => x.ProductsInSale).First(x => x.SaleID == validSaleID).ProductsInSale.Count);

            }
        }
        [TestMethod]
        public void DecreaseQuantityOfAddedProductsInStock_WhenValidParametersArePassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int validSaleID;
            using (var getContext = new StoreSystemDbContext(options))
            {
                validSaleID = getContext.Sales.Include(x => x.ProductsInSale).First(x => x.ProductsInSale.Count == 0).SaleID;
            }

            var validProductName1 = "valid product 1";
            var validProductName2 = "valid product 2";
            decimal expectedQuntityProd1 = 1-1;
            decimal expectedQuntityProd2 = 2-1;

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var validDate = new DateTime(2019, 4, 1);
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                ProductQuantityDto[] products = new[]
                {
                    new ProductQuantityDto(validProductName1,1),
                    new ProductQuantityDto(validProductName2,1),
                };

                //Act
                var isExecuted = sut.AddProductsToSale(validSaleID, products);

                //Assert
                Assert.IsTrue(isExecuted);
                Assert.AreEqual(expectedQuntityProd1, context.Products.Find(1).Quantity);
                Assert.AreEqual(expectedQuntityProd2, context.Products.Find(2).Quantity);

            }
        }

        [TestMethod]
        public void ThrowsArgumentException_WhenInValidSaleIdIsPassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int invalidSaleID = 100;

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var validDate = new DateTime(2019, 4, 1);
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                ProductQuantityDto[] products = new ProductQuantityDto[0];
                var errorText = string.Format(
                                       Consts.ObjectIDNotExist,
                                       nameof(Sale),
                                       invalidSaleID);
                //Act
                //Assert
                Assert.AreEqual(
                    errorText,
                    Assert.ThrowsException<ArgumentException>(() => sut.AddProductsToSale(invalidSaleID, products)).Message);
            }
        }

        [TestMethod]
        public void CreateThrowsArgumentExceptionWithProperMessage_WhenInvalidProductQuantityIsPassed()
        {
            //Arrange
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            var options = DbSeed.GetOptions(databaseName);

            DbSeed.SeedDatabase(options);

            int validSaleID;
            string validProductName1 = "valid product 1";

            using (var getContext = new StoreSystemDbContext(options))
            {
                validSaleID = getContext.Sales.Include(x => x.ProductsInSale).First(x => x.ProductsInSale.Count == 0).SaleID;
            }

            using (var context = new StoreSystemDbContext(options))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();
                var validDate = new DateTime(2019, 4, 1);
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                ProductQuantityDto[] products = new[]
                {
                    new ProductQuantityDto(validProductName1,100),
                };
                var errorText = string.Format(
                       Consts.QuantityNotEnough,
                       validProductName1);

                //Act
                //Assert
                Assert.AreEqual(
                    errorText,
                    Assert.ThrowsException<ArgumentException>(() => sut.AddProductsToSale(validSaleID, products)).Message);

            }
        }
    }
}
