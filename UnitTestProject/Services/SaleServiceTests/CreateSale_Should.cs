﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services;
using StoreSystem.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Tests.Services.SaleServiceTests
{
    [TestClass]
    public class CreateSale_Should
    {
        [TestMethod]
        public void CreateAndAddNewSaleInDB_WhenValidParametersArePassed()
        {
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            using (var context = new StoreSystemDbContext(DbSeed.GetOptions(databaseName)))
            {
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();

                var validDate = new DateTime(2019,4,1);
                var validClientName = "Pesho";

                dateTimeNowProvider.Setup(x => x.Now).Returns(validDate);
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                var client = new Client() { Name = validClientName };
                var address = new Address();
                var city = new City();
                var country = new Country();
                sut.CreateSale(client, 0.15m, 3, address, city, country);
                Assert.AreEqual(1, context.Sales.Count());
                Assert.AreEqual(validClientName, context.Sales.Single(x => x.Client == client).Client.Name);
            }
        }

        [TestMethod]
        public void CreateAndAddNewSaleWithIDsInDB_WhenValidParametersArePassed()
        {
            var databaseName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            using (var context = new StoreSystemDbContext(DbSeed.GetOptions(databaseName)))
            {
                //Arrange
                var dateTimeNowProvider = new Mock<IDateTimeNowProvider>();

                var validDate = new DateTime(2019, 1, 1);

                dateTimeNowProvider.Setup(x => x.Now).Returns(validDate);
                var sut = new SaleService(context, dateTimeNowProvider.Object);
                var client = 1;
                var address = 1;
                var city = 1;
                var country = 1;
                var discount = 0.10;

                //Act
                sut.CreateSale(client, 3, discount, address, city, country);

                //Assert
                Assert.AreEqual(1, context.Sales.Count());
                Assert.AreEqual(client, context.Sales.Single(x => x.ClientID == client).ClientID);
            }
        }
    }
}
