﻿using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace StoreSystem.Services
{
    public class ProductService : IProductService
    {
        private readonly StoreSystemDbContext context;

        public ProductService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Product CreateProduct(string name, string measure, decimal quantity, decimal buyPrice, decimal retailPrice, bool save = true)
        {
            if (this.context.Products.Any(x => x.Name == name))
            {
                throw new ArgumentException("Product exists");
            }
            var newProduct = new Product()
            {
                Name = name,
                Measure = measure,
                Quantity = quantity,
                BuyPrice = buyPrice,
                RetailPrice = retailPrice
            };

            this.context.Products.Add(newProduct);

            if (save) this.context.SaveChanges();

            return newProduct;
        }

        public Product FindProductByName(string productName)
        {
            var product = this.context.Products.FirstOrDefault(p => p.Name == productName);
            return product;
        }

        public Product GetProductByName(string productName)
        {
            var product = this.context.Products.FirstOrDefault(p => p.Name == productName)
                ?? throw new ArgumentException("Product can not be found");
            return product;
        }

        public IReadOnlyCollection<Product> GetAllProducts(int from, int to, string contains = "*", bool haveQuantity = true)
        {
            var query = this.context.Products.AsQueryable();
            if (haveQuantity)
            {
                query = query.Where(x => x.Quantity > 0);
            }

            query = query.OrderBy(x => x.Name).Skip(from).Take(to);

            if (contains != "*")
            {
                query = query.Where(x => x.Name.Contains(contains));
            }

            return query.ToList();
        }

        public bool UpdateProductDetail
            (int productID, string name, string measure, decimal? quantity, decimal? buyPrice, decimal? retailPrice)
        {
            var product = this.context.Products.Find(productID);

            if (name != null)
            {
                if (this.context.Products.Any(x => x.Name == name))
                {
                    throw new ArgumentException($"Product with name \"{name}\" exists");
                }
                product.Name = name;
            }

            if (measure != null) product.Measure = measure;
            if (quantity != null) product.Quantity = (decimal) quantity;
            if (buyPrice != null) product.BuyPrice = (decimal) buyPrice;
            if (retailPrice != null) product.RetailPrice = (decimal) retailPrice;

            var tChanges = this.context.SaveChanges();

            return (tChanges > 0) ? true : false;
        }
    }
}
