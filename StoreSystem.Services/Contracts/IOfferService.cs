﻿using System;
using System.Collections.Generic;
using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface IOfferService
    {
        bool AddProductsToOffer(int offerID, params KeyValuePair<string, decimal>[] productsListWithQuantity);
        Offer CreateOffer(Client client, decimal discount, double daysToDelivery, Address address, City city, Country country, int? offerID = null);
        Offer CreateOffer(int clientId, int addressId, int cityId, int countryId, decimal productDiscount, bool save = true);
        ICollection<Offer> GetAllOffersByClient(string clientName);
        ICollection<ProductOffer> GetAllProductsInOffer(int offerID);
        Offer GetOfferByID(int offerId);
        decimal GetOfferQuantity(int? offerID = null, string clientName = null, int? clientID = null, DateTime? startDate = null, DateTime? endDate = null);
        IReadOnlyCollection<Offer> GetOffersByDate(DateTime startDate, DateTime endDate);
        Offer GetOfferWithClientByID(int offerId);
        Offer GetOfferWithProductsByID(int offerId);
    }
}