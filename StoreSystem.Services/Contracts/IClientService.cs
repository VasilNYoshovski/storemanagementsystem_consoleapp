﻿using System.Collections.Generic;
using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface IClientService
    {
        Client CreateClient(string name, string UIN, string email, string phone, Address address, City city, Country country);
        Client CreateClient(string name, string UIN, string email, string phone, int addressId, int cityId, int countryId, bool save = true);
        Client FindClientByID(int clientID);
        Client FindClientByName(string clientName);
        Client FindClientWithAddress(int clientId);
        Client FindClientWithAddress(string clientName);
        IReadOnlyCollection<Client> GetAllClients(int from, int to, string contains = "*");
        ICollection<Sale> GetClientSales(int clientID);
        ICollection<Sale> GetClientSales(string clientName);
        bool UpdateClient(int clientID, string name, string UIN, string email, string phone, Address address, City city, Country country);
    }
}