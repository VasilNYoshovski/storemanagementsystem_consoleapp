﻿using System.Collections.Generic;
using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface IAddressService
    {
        Address CreateAddress(string addressString, bool save = true);
        Address FindAddresByID(int addressID);
        Address FindAddressByName(string addressString);
        ICollection<Client> GetListOfAllClientsbyID(int addressID);
    }
}