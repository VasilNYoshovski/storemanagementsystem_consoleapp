﻿using System.Collections.Generic;
using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface ICityService
    {
        City CreateCity(string cityString, bool save = true);
        City FindCityByID(int cityID);
        City FindCityByName(string CityString);
        ICollection<Client> GetListOfAllClientsbyName(string cityName);
    }
}