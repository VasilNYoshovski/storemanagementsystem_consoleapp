﻿using System.Collections.Generic;
using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface IProductService
    {
        Product CreateProduct(string name, string measure, decimal quantity, decimal buyPrice, decimal retailPrice, bool save = true);
        Product FindProductByName(string productName);
        IReadOnlyCollection<Product> GetAllProducts(int from, int to, string contains = "*", bool haveQuantity = true);
        Product GetProductByName(string productName);
        bool UpdateProductDetail(int productID, string name, string measure, decimal? quantity, decimal? buyPrice, decimal? retailPrice);
    }
}