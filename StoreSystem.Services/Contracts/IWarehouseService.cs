﻿using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface IWarehouseService
    {
        bool AddPurchaseToWarehouse(int warehouseID, int purchaseID);
        Warehouse CreateWarehouse(string warehouseName, int countryID, int cityID, int addressID, bool save = true);
        Warehouse FindWarehouseByID(int id);
        Warehouse FindWarehouseByName(string warehouseName);
        Warehouse GetWarehouseByName(string warehouseName);
    }
}