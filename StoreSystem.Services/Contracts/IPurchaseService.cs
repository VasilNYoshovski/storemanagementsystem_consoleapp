﻿using System;
using System.Collections.Generic;
using StoreSystem.Data.Models;
using StoreSystem.Services.Dto;

namespace StoreSystem.Services
{
    public interface IPurchaseService
    {
        bool AddProductsToPurchase(int PurchaseID, params ProductQuantityDto[] productsListWithQuantity);
        Purchase CreatePurchase(int supplierId, int warehouseId, double daysToDelivery, bool save = true);
        Purchase FindPurchaseByID(int id);
        decimal GetPurchaseQuantity
            (int? purchaseID = null, string supplierName = null, int? supplierID = null,
            DateTime? startDate = null, DateTime? endDate = null);
    }
}