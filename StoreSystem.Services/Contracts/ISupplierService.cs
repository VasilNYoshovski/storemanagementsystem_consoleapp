﻿using System.Collections.Generic;
using StoreSystem.Data.Models;
using StoreSystem.Services.Dto;

namespace StoreSystem.Services
{
    public interface ISupplierService
    {
        bool AddProductsToSupplier(int supplierID, params string[] productsListWithQuantity);
        bool AddProductToSupplier(int supplierID, int productID);
        bool AddPurchaseToSupplier(int supplierID, int purchaseID);
        Supplier CreateSupplier(string supplierName, string uin, string email, string phone, int countryId, int cityId, int addressId, bool save = true);
        Supplier FindSupplierByID(int id);
        Supplier FindSupplierByName(string warehouseName);
        IReadOnlyCollection<Supplier> GetAllSuppliers(int from, int to, string contains = "*");
        Supplier GetSupplierByName(string warehouseName);
        bool AddPurchasesToSupplier(int supplierID, int[] puchasesList);
        ICollection<Purchase> GetSupplierPurchases(int supplierID);
        ICollection<Purchase> GetSupplierPurchases(string supplierName);
    }
}