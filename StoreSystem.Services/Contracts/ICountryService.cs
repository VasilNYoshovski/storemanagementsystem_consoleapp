﻿using System.Collections.Generic;
using StoreSystem.Data.Models;

namespace StoreSystem.Services
{
    public interface ICountryService
    {
        Country CreateCountry(string CountryString, bool save = true);
        Country FindCountryByID(int countryID);
        Country FindCountryByName(string CountryString);
        ICollection<Client> GetListOfAllClientsbyName(string countryName);
    }
}