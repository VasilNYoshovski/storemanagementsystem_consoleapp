﻿using System;
using System.Collections.Generic;
using StoreSystem.Data.Models;
using StoreSystem.Services.Dto;

namespace StoreSystem.Services
{
    public interface ISaleService
    {
        bool AddProductsByIdToSale(int saleID, params ProductIdQuantityDto[] productsIdListWithQuantity);
        bool AddProductsToSale(int saleID, params ProductQuantityDto[] productsListWithQuantity);
        Sale CreateSale(Client client, decimal discount, double daysToDelivery, Address address, City city, Country country, int? offerID = null, bool save = true);
        Sale CreateSale(int clientId, decimal discount, double daysToDelivery, int addressId, int cityId, int countryId, int? offerID = null);
        Sale CreateSaleByOfferID(int offerID);
        bool DeleteSale(int saleID);
        ICollection<ProductSale> GetAllProductsInSale(int saleID);
        ICollection<Sale> GetNotCLosedSales();
        Sale GetSaleByID(int saleId);
        decimal GetSaleQuantity(int? saleID = null, string clientName = null, int? clientID = null, DateTime? startDate = null, DateTime? endDate = null);
        IReadOnlyCollection<Sale> GetSalesByDate(DateTime startDate, DateTime endDate);
        Sale GetSaleWithProductsByID(int saleId);
        bool UpdateSaleDeliveryDate(int saleID, DateTime deliveryDate);
    }
}