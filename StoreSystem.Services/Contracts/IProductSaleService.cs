﻿using System.Collections.Generic;
using System.Linq;
using StoreSystem.Services.Dto;

namespace StoreSystem.Services
{
    public interface IProductSaleService
    {
        List<IGrouping<int, ProductTotalDto>> GetProductsTotalSaleQuantity(string alies = "*");
    }
}