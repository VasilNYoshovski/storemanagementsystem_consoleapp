﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Services
{
    public class AddressService : IAddressService
    {
        private readonly StoreSystemDbContext context;

        public AddressService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Address FindAddressByName(string addressString)
        {
            return this.context.Addresses.FirstOrDefault(x => x.Name == addressString);
        }

        public Address FindAddresByID(int addressID)
        {
            return this.context.Addresses.Find(addressID);
        }

        public Address CreateAddress(string addressString, bool save = true)
        {
            var address = this.context.Addresses.FirstOrDefault(x => x.Name == addressString);

            if (address == null)
            {
                address = new Address { Name = addressString };
                context.Addresses.Add(address);
                if (save) context.SaveChanges();
            }
            return address;
        }

        public ICollection<Client> GetListOfAllClientsbyID(int addressID)
        {
            var address = this.context.Addresses.Include(x => x.Clients)
                .FirstOrDefault(x => x.AddressID == addressID) ?? throw new ArgumentException($"There is not address with ID \"{addressID}\"!");

            return address.Clients;
        }
    }
}
