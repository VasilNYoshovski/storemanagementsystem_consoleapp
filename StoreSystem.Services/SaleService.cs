﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services.Dto;
using StoreSystem.Services.Providers;
using StoreSystem.Services.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Services
{

    public class SaleService : ISaleService
    {
        private readonly StoreSystemDbContext context;
        private readonly IDateTimeNowProvider dateNow;

        public SaleService(StoreSystemDbContext context, IDateTimeNowProvider dateNow)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateNow = dateNow ?? throw new ArgumentNullException(nameof(dateNow));
        }

        public Sale CreateSale(
            Client client,
            decimal discount,
            double daysToDelivery,
            Address address,
            City city,
            Country country,
            int? offerID = null,
            bool save = true)
        {
            var newSale = new Sale()
            {
                Client = client,
                ProductDiscount = discount,
                OrderDate = this.dateNow.Now.Date,
                DeadlineDate = this.dateNow.Now.AddDays(daysToDelivery),
                DeliveryAddress = address,
                DeliveryCity = city,
                DeliveryCountry = country,
                OfferID = offerID
            };
            this.context.Sales.Add(newSale);
            this.context.SaveChanges();
            return newSale;
        }

        public Sale CreateSale(
            int clientId,
            decimal discount,
            double daysToDelivery,
            int addressId,
            int cityId,
            int countryId,
            int? offerID = null)
        {
            var newSale = new Sale()
            {
                ClientID = clientId,
                OrderDate = this.dateNow.Now.Date,
                DeadlineDate = this.dateNow.Now.AddDays(daysToDelivery),
                AddressID = addressId,
                CityID = cityId,
                CountryID = countryId,
                OfferID = offerID
            };
            this.context.Sales.Add(newSale);
            this.context.SaveChanges();
            return newSale;
        }

        public Sale CreateSaleByOfferID(int offerID)
        {
            var offer = this.context.Offers.Find(offerID) ?? throw new ArgumentException(string.Format(
                                                                            Consts.ObjectIDNotExist,
                                                                            nameof(Offer),
                                                                            offerID));

            return this.CreateSale(offer.Client, offer.ProductDiscount, (offer.ExpiredDate - offer.OfferDate).Days, offer.DeliveryAddress, offer.DeliveryCity, offer.DeliveryCountry, offerID);
        }

        public bool AddProductsToSale(int saleID, params ProductQuantityDto[] productsListWithQuantity)
        {
            var sale = this.context.Sales.Find(saleID) ?? throw new ArgumentException(string.Format(
                                                                            Consts.ObjectIDNotExist,
                                                                            nameof(Sale),
                                                                            saleID));
            Product product;
            sale.ProductsInSale = productsListWithQuantity.Select(pwq =>
            {
                product = this.context.Products.FirstOrDefault(x => x.Name == pwq.Name) ?? throw new ArgumentException(string.Format(
                                                                            Consts.ObjectNameNotExist,
                                                                            nameof(Product),
                                                                            pwq.Name));
                if (product.Quantity < pwq.Quantity)
                {
                    throw new ArgumentException(string.Format(
                                      Consts.QuantityNotEnough,
                                      pwq.Name));
                }
                product.Quantity -= pwq.Quantity;
                return new ProductSale() { Product = product, Quantity = pwq.Quantity };

            }).ToList();
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public bool AddProductsByIdToSale(int saleID, params ProductIdQuantityDto[] productsIdListWithQuantity)
        {
            var sale = this.context.Sales.Find(saleID) ?? throw new ArgumentException(string.Format(
                                                                            Consts.ObjectIDNotExist,
                                                                            nameof(Sale),
                                                                            saleID));
            Product product;
            sale.ProductsInSale = productsIdListWithQuantity.Select(pwq =>
            {
                product = this.context.Products.Find(pwq.ProductID) ?? throw new ArgumentException(string.Format(
                                                                            Consts.ObjectIDNotExist,
                                                                            nameof(Product),
                                                                            pwq.ProductID));
                if (product.Quantity < pwq.Quantity)
                {
                    throw new ArgumentException(string.Format(
                                      Consts.QuantityNotEnough,
                                      pwq.ProductID));
                }
                product.Quantity -= pwq.Quantity;
                return new ProductSale() { Product = product, Quantity = pwq.Quantity };

            }).ToList();
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public Sale GetSaleWithProductsByID(int saleId)
        {
            return this.context.Sales
                .Include(s => s.ProductsInSale)
                .FirstOrDefault(o => o.SaleID == saleId);
        }

        public Sale GetSaleByID(int saleId)
        {
            return this.context.Sales.Find(saleId) ?? throw new ArgumentException(string.Format(
                                                                            Consts.ObjectIDNotExist,
                                                                            nameof(Sale),
                                                                            saleId));
        }

        public ICollection<ProductSale> GetAllProductsInSale(int saleID)
        {
            var sale = this.context.Sales
                .Include(x => x.ProductsInSale)
                .ThenInclude(x => x.Product)
                .FirstOrDefault(x => x.SaleID == saleID)
                    ?? throw new ArgumentException(string.Format(
                                         Consts.ObjectIDNotExist,
                                         nameof(Sale),
                                         saleID));

            return sale.ProductsInSale.ToList();
        }

        public IReadOnlyCollection<Sale> GetSalesByDate(DateTime startDate, DateTime endDate)
        {
            return this.context.Sales
                .Include(x => x.ProductsInSale)
                .Where(x => x.OrderDate >= startDate && x.OrderDate <= endDate)
                .ToList();
        }

        //public decimal GetSaleQuantityByDate(DateTime startDate, DateTime endDate)
        //{
        //    return this.context.Sales
        //        .Include(x => x.ProductsInSale)
        //        .ThenInclude(x => x.Product)
        //        .Where(x => x.OrderDate >= startDate && x.OrderDate <= endDate)
        //        .SelectMany(x => x.ProductsInSale)
        //        .Select(x => x.Quantity * x.Product.RetailPrice * (1 - x.Sale.ProductDiscount))
        //        .Sum();
        //}

        //public decimal GetSaleQuantityByID(int saleID)
        //{
        //    return this.context.Sales
        //        .Include(x => x.ProductsInSale)
        //        .ThenInclude(x => x.Product)
        //        .Where(x => x.SaleID == saleID)
        //        .SelectMany(x => x.ProductsInSale)
        //        .Select(x => x.Quantity * x.Product.RetailPrice * (1 - x.Sale.ProductDiscount))
        //        .Sum();
        //}

        public decimal GetSaleQuantity
            (int? saleID = null, string clientName = null, int? clientID = null,
            DateTime? startDate = null, DateTime? endDate = null)
        {
            var query = this.context.Sales
                .Include(x => x.ProductsInSale)
                .ThenInclude(x => x.Product).AsQueryable();

            if (saleID != null) query = query.Where(x => x.SaleID == saleID);

            if (clientName != null)
            {
                query = query.Include(x => x.Client);
                query = query.Where(x => x.Client.Name == clientName);
            }

            if (clientID != null)
            {
                query = query.Include(x => x.Client);
                query = query.Where(x => x.Client.ClientID == clientID);
            }

            if (startDate != null && endDate != null)
                query = query.Where(x => x.OrderDate >= startDate && x.OrderDate <= endDate);

            return query
                .SelectMany(x => x.ProductsInSale)
                .Select(x => x.Quantity * x.Product.RetailPrice * (1 - x.Sale.ProductDiscount))
                .Sum();
        }

        public bool UpdateSaleDeliveryDate(int saleID, DateTime deliveryDate)
        {
            var sale = this.context.Sales.Find(saleID)
                    ?? throw new ArgumentException(string.Format(
                                         Consts.ObjectIDNotExist,
                                         nameof(Sale),
                                         saleID));

            if (sale.OrderDate > deliveryDate.Date)
            {
                throw new ArgumentException(string.Format(
                                  Consts.DateError,
                                  deliveryDate.Date,
                                  sale.OrderDate.Date));
            }
            sale.DeliveryDate = deliveryDate.Date;
            var tChanges = this.context.SaveChanges();
            return (tChanges > 0) ? true : false;
        }

        public ICollection<Sale> GetNotCLosedSales()
        {
            return this.context.Sales.Where(x => x.DeliveryDate < x.OrderDate).ToList();
        }

        public bool DeleteSale(int saleID)
        {
            var sale = this.context.Sales.Find(saleID)
                    ?? throw new ArgumentException(string.Format(
                                         Consts.ObjectIDNotExist,
                                         nameof(Sale),
                                         saleID));

            this.context.Sales.Remove(sale);

            this.context.ProductSales.Where(x => x.SaleID == saleID).ToList()
                .ForEach(x =>
                {
                    context.Products.Find(x.ProductID).Quantity += x.Quantity;
                    this.context.ProductSales.Remove(x);
                }
                );

            var tChanges = this.context.SaveChanges();
            return (tChanges > 0) ? true : false;
        }


    }
}
