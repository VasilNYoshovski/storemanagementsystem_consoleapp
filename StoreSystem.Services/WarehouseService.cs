﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using System;
using System.Linq;

namespace StoreSystem.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly StoreSystemDbContext context;

        public WarehouseService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Warehouse FindWarehouseByID(int id)
        {
            return this.context.Warehouses.Find(id);
        }

        public Warehouse FindWarehouseByName(string warehouseName)
        {
            return this.context.Warehouses.FirstOrDefault(x => x.Name == warehouseName);
        }

        public Warehouse GetWarehouseByName(string warehouseName)
        {
            return this.context.Warehouses.FirstOrDefault(x => x.Name == warehouseName) ?? throw new ArgumentException($"Can not find warehouse with name {warehouseName}");
        }

        public Warehouse CreateWarehouse(string warehouseName, int countryID, int cityID, int addressID, bool save = true)
        {
            if (string.IsNullOrEmpty(warehouseName))
            {
                throw new ArgumentNullException($"WarehouseName could not be null or empty!");
            }

            if (string.IsNullOrWhiteSpace(warehouseName))
            {
                throw new ArgumentException($"WarehouseName could not be WhiteSpace!");
            }

            var Warehouse = this.context.Warehouses.FirstOrDefault(x => x.Name == warehouseName);
            if (null == this.context.Countries.FirstOrDefault(x => x.CountryID == countryID))
            {
                throw new ArgumentException($"Country with ID {countryID} does not exist!");
            }
            if (null == this.context.Cities.FirstOrDefault(x => x.CityID == cityID))
            {
                throw new ArgumentException($"City with ID {countryID} does not exist!");
            }
            if (null == this.context.Addresses.FirstOrDefault(x => x.AddressID == addressID))
            {
                throw new ArgumentException($"Address with ID {countryID} does not exist!");
            }

            if (Warehouse == null)
            {
                Warehouse = new Warehouse {
                    CountryID = countryID,
                    Name = warehouseName,
                    CityID = cityID,
                    AddressID = addressID
                };
                this.context.Warehouses.Add(Warehouse);
                this.context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Warehouse with name {warehouseName} already exists!");
            }
            return Warehouse;
        }

        public bool AddPurchaseToWarehouse(int warehouseID, int purchaseID)
        {
            var warehouse = this.context.Warehouses
                .Include(p => p.Purchases)
                .Where(w => w.WarehouseID == warehouseID)
                .FirstOrDefault() ?? throw new ArgumentException($"Can not find warehouse with ID: {warehouseID}.");
            if (warehouse.Purchases.Any(x => x.PurchaseID == purchaseID))
            {
                throw new ArgumentException($"Purchase with ID: {purchaseID} already included for warehouse with ID: {warehouseID}.");
            }
            var purchase = this.context.Purchases.Find(purchaseID) ?? throw new ArgumentException($"Can not find purchase with ID: {purchaseID}.");
            warehouse.Purchases.Add(purchase);
            return this.context.SaveChanges() > 0 ? true : false;
        }
    }
}
