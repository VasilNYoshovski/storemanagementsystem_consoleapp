﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Services
{
    public class CityService : ICityService
    {
        private readonly StoreSystemDbContext context;

        public CityService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public City FindCityByName(string CityString)
        {
            return this.context.Cities.FirstOrDefault(x => x.Name == CityString);
        }

        public City FindCityByID(int cityID)
        {
            return this.context.Cities.Find(cityID);
        }

        public City CreateCity(string cityString, bool save = true)
        {
            var city = this.context.Cities.FirstOrDefault(x => x.Name == cityString);

            if (city == null)
            {
                city = new City { Name = cityString };
                this.context.Cities.Add(city);
                if (save) this.context.SaveChanges();
            }
            return city;
        }

        public ICollection<Client> GetListOfAllClientsbyName(string cityName)
        {
            var city = this.context.Cities.Include(x => x.Clients)
                .FirstOrDefault(x => x.Name == cityName) ?? throw new ArgumentException($"There is not city with name \"{cityName}\"!");

            return city.Clients;
        }
    }
}
