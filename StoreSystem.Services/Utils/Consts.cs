﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Services.Utils
{
    class Consts
    {
        internal static string ObjectIDNotExist = "Can not find {0} with ID {1}.";
        internal static string ObjectNameNotExist = "Can not find {0} with name {1}.";

        internal static string QuantityNotEnough = "There is not enough quantity of {0}";
        internal static string DateError = "Invalid date. Delivery date {0} can not be before order date {0}";

        internal const string AllText = "*";
    }
}
