﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Services
{
    public class CountryService : ICountryService
    {
        private readonly StoreSystemDbContext context;

        public CountryService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Country FindCountryByID(int countryID)
        {
            return this.context.Countries.Find(countryID);
        }

        public Country FindCountryByName(string CountryString)
        {
            return this.context.Countries.FirstOrDefault(x => x.Name == CountryString);
        }

        public Country CreateCountry(string CountryString, bool save = true)
        {
            var Country = this.context.Countries.FirstOrDefault(x => x.Name == CountryString);

            if (Country == null)
            {
                Country = new Country { Name = CountryString };
                this.context.Countries.Add(Country);
                if (save) this.context.SaveChanges();
            }
            return Country;
        }

        public ICollection<Client> GetListOfAllClientsbyName(string countryName)
        {
            var country = this.context.Countries.Include(x => x.Clients)
                .FirstOrDefault(x => x.Name == countryName) ?? throw new ArgumentException($"There is not country with name \"{countryName}\"!");

            return country.Clients;
        }
    }
}
