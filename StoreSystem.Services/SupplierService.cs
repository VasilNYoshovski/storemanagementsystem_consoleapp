﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StoreSystem.Services
{
    public class SupplierService : ISupplierService
    {
        private readonly StoreSystemDbContext context;

        public SupplierService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Supplier FindSupplierByID(int id)
        {
            return this.context.Suppliers
                .Include(s => s.ProductsOfSupplier)
                .FirstOrDefault(s => s.SupplierID == id);
        }

        public Supplier FindSupplierByName(string warehouseName)
        {
            return this.context.Suppliers.FirstOrDefault(x => x.Name == warehouseName);
        }

        public Supplier GetSupplierByName(string warehouseName)
        {
            return this.context.Suppliers.FirstOrDefault(x => x.Name == warehouseName) ?? throw new ArgumentException($"Can not find warehouse with name {warehouseName}");
        }

        public Supplier CreateSupplier(
            string supplierName,
            string uin,
            string email,
            string phone,
            int countryId,
            int cityId,
            int addressId,
            bool save = true)
        {
            var Supplier = this.context.Suppliers.FirstOrDefault(x => x.Name == supplierName);

            if (Supplier == null)
            {
                Supplier = new Supplier {
                    Name = supplierName,
                    UIN = uin,
                    Email = email,
                    Phone = phone,
                    CountryID = countryId,
                    CityID = cityId,
                    AddressID = addressId
                };
                this.context.Suppliers.Add(Supplier);
                this.context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Supplier with name {supplierName} already exists!");
            }
            return Supplier;
        }

        public bool AddPurchaseToSupplier(int supplierID, int purchaseID)
        {
            var supplier = this.context.Suppliers
                .Include(s => s.Purchases)
                .Include(s => s.ProductsOfSupplier)
                .FirstOrDefault(s => s.SupplierID == supplierID) ?? throw new ArgumentException($"Can not find supplier with ID: {supplierID}.");
            if (supplier.Purchases == null)
            {
                supplier.Purchases = new List<Purchase>();
            }
            if (supplier.Purchases.Any(x => x.PurchaseID == purchaseID))
            {
                //return true;
                throw new ArgumentException($"Purchase with ID: {purchaseID} already included for supplier with ID: {supplierID}.");
            }
            var purchase = this.context.Purchases
                .Include(p => p.ProductsТоPurchase)
                .FirstOrDefault(p => p.PurchaseID == purchaseID) ?? throw new ArgumentException($"Can not find purchase with ID: {purchaseID}.");
            supplier.Purchases.Add(purchase);
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public bool AddPurchasesToSupplier(int supplierID, int[] puchasesList)
        {
            var existingPurchases = this.context.Purchases.ToArray();
            var supplier = this.context.Suppliers
                .Include(s => s.Purchases)
                .FirstOrDefault(s => s.SupplierID == supplierID) ?? throw new ArgumentException($"Can not find supplier with ID: {supplierID}.");
            if (supplier.Purchases == null)
            {
                supplier.Purchases = new List<Purchase>();
            }
            foreach (var purchaseId in puchasesList)
            {
                if (existingPurchases.All(x => x.PurchaseID != purchaseId))
                {
                    throw new ArgumentException($"Purchase with ID: {purchaseId} does not exist.");
                }
                if (supplier.Purchases.Any(x => x.PurchaseID == purchaseId))
                {
                    throw new ArgumentException($"Purchase with ID: {purchaseId} already included for supplier with ID: {supplierID}.");
                }
                // Add the purchases
                supplier.Purchases.Add(existingPurchases.Where(e => e.PurchaseID == purchaseId).FirstOrDefault());
            }
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public bool AddProductsToSupplier(int supplierID, params string[] productsListWithQuantity)
        {
            var supplier = this.context.Suppliers.Find(supplierID) ?? throw new ArgumentException($"Can not find supplier with ID {supplierID}.");
            Product product;
            List<ProductSupplier> psList = new List<ProductSupplier>();
            psList = productsListWithQuantity.Select(pwq =>
            {
                product = this.context.Products.FirstOrDefault(x => x.Name == pwq) ?? throw new ArgumentException($"Can not find product {pwq}");
                var productsOfSupp = this.context.productSupplier.FirstOrDefault(x => ((x.ProductID == product.ProductID) && (x.SupplierID == supplierID)));
                if (null != productsOfSupp)
                {
                    return null;
                }
                return new ProductSupplier() { Product = product, Supplier = supplier };

            }).ToList();
            foreach (var item in psList)
            {
                if (item != null)
                {
                    supplier.ProductsOfSupplier.Add(item);
                }
                else
                {
                    ;
                }
            }
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public bool AddProductToSupplier(int supplierID, int productID)
        {
            var supplier = this.context.Suppliers
                .Include(s => s.Purchases)
                .Include(s => s.ProductsOfSupplier)
                .FirstOrDefault(x => x.SupplierID == supplierID) ?? throw new ArgumentException($"Can not find supplier with ID: {supplierID}.");
            if (supplier.ProductsOfSupplier == null)
            {
                supplier.ProductsOfSupplier = new List<ProductSupplier>();
            }
            if (supplier.ProductsOfSupplier.Any(x => x.ProductID == productID))
            {
                //return true;
                throw new ArgumentException($"Product with ID: {productID} already included for supplier with ID: {supplierID}.");
            }
            var product = this.context.Products.Find(productID) ?? throw new ArgumentException($"Can not find product with ID: {productID}.");
            var productSupplierToAdd = new ProductSupplier()
            {
                ProductID = productID,
                SupplierID = supplierID
            };
            supplier.ProductsOfSupplier.Add(productSupplierToAdd);
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public IReadOnlyCollection<Supplier> GetAllSuppliers(int from, int to, string contains = "*")
        {
            var query = this.context.Suppliers.OrderBy(x => x.Name).Skip(from).Take(to);
            if (contains != "*")
            {
                query = query.Where(x => x.Name.Contains(contains));
            }
            return query.ToList();
        }

        public ICollection<Purchase> GetSupplierPurchases(int supplierID)
        {
            var supplier = context.Suppliers
                .Include(x => x.Purchases)
                .FirstOrDefault(x => x.SupplierID == supplierID) // We use FIrstOrDefault because it's faster than SIngleOrDefault
                ?? throw new ArgumentException($"Supplier with ID {supplierID} can not be found");
            return supplier.Purchases;
        }

        public ICollection<Purchase> GetSupplierPurchases(string supplierName)
        {
            var supplier = context.Suppliers
                .Include(x => x.Purchases)
                .FirstOrDefault(x => x.Name == supplierName) // We use FIrstOrDefault because it's faster than SIngleOrDefault
                ?? throw new ArgumentException($"Supplier with name {supplierName} can not be found");
            return supplier.Purchases;
        }
    }
}
