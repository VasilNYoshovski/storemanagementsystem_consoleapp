﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Services
{
    public class OfferService : IOfferService
    {
        private readonly StoreSystemDbContext context;
        private readonly IDateTimeNowProvider dateNow;

        public OfferService(StoreSystemDbContext context, IDateTimeNowProvider dateNow)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateNow = dateNow ?? throw new ArgumentNullException(nameof(dateNow));
        }

        public Offer CreateOffer(
            Client client,
            decimal discount,
            double daysToDelivery,
            Address address,
            City city,
            Country country,
            int? offerID = null)
        {
            var newOffer = new Offer()
            {
                Client = client,
                ProductDiscount = discount,
                OfferDate = this.dateNow.Now,
                ExpiredDate = this.dateNow.Now.AddDays(daysToDelivery),
                DeliveryAddress = address,
                DeliveryCity = city,
                DeliveryCountry = country,
            };
            this.context.Offers.Add(newOffer);
            this.context.SaveChanges();
            return newOffer;
        }

        public Offer CreateOffer(
            int clientId,
            int addressId,
            int cityId,
            int countryId,
            decimal productDiscount,
            bool save = true)
        {
            var newOffer = new Offer()
            {
                ClientID = clientId,
                AddressID = addressId,
                CityID = cityId,
                CountryID = countryId,
                OfferDate = this.dateNow.Now,
                DeliveryDate = this.dateNow.Now,
                ExpiredDate = this.dateNow.Now,
                ProductDiscount = productDiscount
            };
            this.context.Offers.Add(newOffer);
            this.context.SaveChanges();
            return newOffer;
        }

        public Offer GetOfferByID(int offerId)
        {
            var offer = this.context.Offers.Find(offerId) ?? throw new ArgumentException($"Can not find offer with ID {offerId}.");

            return offer;
        }

        public Offer GetOfferWithClientByID(int offerId)
        {
            var offer = this.context.Offers.Include(x => x.Client).FirstOrDefault(x => x.OfferID == offerId) ?? throw new ArgumentException($"Can not find offer with ID {offerId}.");

            return offer;
        }

        public Offer GetOfferWithProductsByID(int offerId)
        {
            return this.context.Offers
                .Include(o => o.ProductsInOffer)
                .FirstOrDefault(o => o.OfferID == offerId);
        }

        public bool AddProductsToOffer(int offerID, params KeyValuePair<string, decimal>[] productsListWithQuantity)
        {
            var offer = this.context.Offers.Find(offerID) ?? throw new ArgumentException($"Can not find offer with ID {offerID}.");
            Product product;
            offer.ProductsInOffer = productsListWithQuantity.Select(pwq =>
            {
                product = this.context.Products.FirstOrDefault(x => x.Name == pwq.Key) ?? throw new ArgumentException($"Can not find product {pwq.Key}");
                if (product.Quantity < pwq.Value)
                {
                    throw new ArgumentException($"There is not enought quantity of {pwq.Key}");
                }
                return new ProductOffer() { Product = product, Quantity = pwq.Value };

            }).ToList();
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public ICollection<Offer> GetAllOffersByClient(string clientName)
        {
            return this.context.Offers
                .Include(x => x.ProductsInOffer)
                .ThenInclude(x => x.Product)
                .Where(x => x.Client.Name == clientName)
                .ToList();
        }

        public ICollection<ProductOffer> GetAllProductsInOffer(int offerID)
        {
            var offer = this.context.Offers
                .Include(x => x.ProductsInOffer)
                .ThenInclude(x => x.Product)
                .FirstOrDefault(x => x.OfferID == offerID)
            ?? throw new ArgumentException($"There is not offer with ID {offerID}");

            return offer.ProductsInOffer.ToList();
        }

        public IReadOnlyCollection<Offer> GetOffersByDate(DateTime startDate, DateTime endDate)
        {
            return this.context.Offers
                .Include(x => x.ProductsInOffer)
                .Where(x => x.OfferDate >= startDate && x.OfferDate <= endDate)
                .ToList();
        }

        public decimal GetOfferQuantity
            (int? offerID = null, string clientName = null, int? clientID = null,
            DateTime? startDate = null, DateTime? endDate = null)
        {
            var query = this.context.Offers
                .Include(x => x.ProductsInOffer)
                .ThenInclude(x => x.Product).AsQueryable();

            if (offerID != null) query = query.Where(x => x.OfferID == offerID);

            if (clientName != null)
            {
                query = query.Include(x => x.Client);
                query = query.Where(x => x.Client.Name == clientName);
            }

            if (clientID != null)
            {
                query = query.Include(x => x.Client);
                query = query.Where(x => x.Client.Name == clientName);
            }

            if (startDate != null && endDate != null)
                query = query.Where(x => x.OfferDate >= startDate && x.OfferDate <= endDate);

            return query
                .SelectMany(x => x.ProductsInOffer)
                .Select(x => x.Quantity * x.Product.RetailPrice * (1 - x.Offer.ProductDiscount))
                .Sum();
        }
    }
}
