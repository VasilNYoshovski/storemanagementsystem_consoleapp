﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Services
{
    public class ClientService : IClientService
    {
        private readonly StoreSystemDbContext context;

        public ClientService(StoreSystemDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Client CreateClient(string name, string UIN, string email, string phone, Address address, City city, Country country)
        {
            if (this.context.Clients.Any(x => x.Name == name))
            {
                throw new ArgumentException($"There is client with name {name}");
            }
            var client = new Client()
            {
                Name = name,
                EIK = UIN,
                Email = email,
                Phone = phone,
                Address = address,
                City = city,
                Country = country,
            };
            this.context.Clients.Add(client);
            this.context.SaveChanges();
            return client;
        }

        public Client CreateClient(string name, string UIN, string email, string phone, int addressId, int cityId, int countryId, bool save = true)
        {
            if (this.context.Clients.Any(x => x.Name == name))
            {
                throw new ArgumentException($"There is client with name {name}");
            }
            var client = new Client()
            {
                Name = name,
                EIK = UIN,
                Email = email,
                Phone = phone,
                AddressID = addressId,
                CityID = cityId,
                CountryID = countryId,
            };
            this.context.Clients.Add(client);
            this.context.SaveChanges();
            return client;
        }

        public Client FindClientByName(string clientName)
        {
            return this.context.Clients.FirstOrDefault(x => x.Name == clientName);
        }

        public Client FindClientByID(int clientID)
        {
            return this.context.Clients.Find(clientID);
        }

        public Client FindClientWithAddress(string clientName)
        {
            return this.context.Clients
                               .Include(x => x.Address)
                               .Include(x => x.City)
                               .Include(x => x.Country)
                               .FirstOrDefault(x => x.Name == clientName);
        }

        public Client FindClientWithAddress(int clientId)
        {
            return this.context.Clients
                               .Include(x => x.Address)
                               .Include(x => x.City)
                               .Include(x => x.Country)
                               .FirstOrDefault(x => x.ClientID == clientId);
        }

        public IReadOnlyCollection<Client> GetAllClients(int from, int to, string contains = "*")
        {
            var query = this.context.Clients.OrderBy(x => x.Name).Skip(from).Take(to);
            if (contains != "*")
            {
                query = query.Where(x => x.Name.Contains(contains));
            }
            return query.ToList();
        }

        public bool UpdateClient(int clientID, string name, string UIN, string email, string phone, Address address, City city, Country country)
        {
            const string nonModify = "-";

            var client = this.context.Clients.Find(clientID);

            if (name != nonModify)
            {
                if (this.context.Clients.Any(x => x.Name == name))
                {
                    throw new ArgumentException($"There is client with name {name}");
                }
                client.Name = name;
            }

            if (UIN != nonModify) client.EIK = UIN;
            if (email != nonModify) client.Email = email;
            if (phone != nonModify) client.Phone = phone;
            if (address != null) client.Address = address;
            if (city != null) client.City = city;
            if (country != null) client.Country = country;

            return this.context.SaveChanges() > 0 ? true : false;
        }

        public ICollection<Sale> GetClientSales(int clientID)
        {
            var client = context.Clients
                .Include(x => x.Sales)
                .FirstOrDefault(x => x.ClientID == clientID) // We use FIrstOrDefault because it's faster than SIngleOrDefault
                ?? throw new ArgumentException($"Client with ID {clientID} can not be found");
            return client.Sales;
        }

        public ICollection<Sale> GetClientSales(string clientName)
        {
            var client = context.Clients
                .Include(x => x.Sales)
                .FirstOrDefault(x => x.Name == clientName) // We use FIrstOrDefault because it's faster than SIngleOrDefault
                ?? throw new ArgumentException($"Client with name {clientName} can not be found");
            return client.Sales;
        }

    }
}
