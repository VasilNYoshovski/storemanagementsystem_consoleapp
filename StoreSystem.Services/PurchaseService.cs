﻿using Microsoft.EntityFrameworkCore;
using StoreSystem.Data.DbContext;
using StoreSystem.Data.Models;
using StoreSystem.Services.Dto;
using StoreSystem.Services.Providers;
using System;
using System.Linq;

namespace StoreSystem.Services
{
    public class PurchaseService : IPurchaseService
    {
        private readonly StoreSystemDbContext context;
        private readonly IDateTimeNowProvider dateNow;

        public PurchaseService(StoreSystemDbContext context, IDateTimeNowProvider dateNow)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateNow = dateNow ?? throw new ArgumentNullException(nameof(dateNow));
        }

        public Purchase CreatePurchase(
            int supplierId,
            int warehouseId,
            double daysToDelivery,
            bool save = true)
        {
            var newPurchase = new Purchase()
            {
                SupplierID = supplierId,
                WarehouseID = warehouseId,
                PurchaseDate = this.dateNow.Now,
                DeliveryDate = this.dateNow.Now,
                DeadlineDate = this.dateNow.Now.AddDays(daysToDelivery),
            };
            context.Purchases.Add(newPurchase);
            context.SaveChanges();
            return newPurchase;
        }

        public Purchase FindPurchaseByID(int id)
        {
            return this.context.Purchases
                .Include(p => p.ProductsТоPurchase)
                .Include(p => p.Warehouse)
                .FirstOrDefault(p => p.PurchaseID == id);
        }

        public bool AddProductsToPurchase(int PurchaseID, params ProductQuantityDto[] productsListWithQuantity)
        {
            var purchase = this.context.Purchases.Find(PurchaseID) ?? throw new ArgumentException($"Can not find purchase with ID {PurchaseID}.");
            Product product;
            purchase.ProductsТоPurchase = productsListWithQuantity.Select(pwq =>
            {
                product = this.context.Products.FirstOrDefault(x => x.Name == pwq.Name) ?? throw new ArgumentException($"Can not find product {pwq.Name}");
                product.Quantity += pwq.Quantity;
                return new ProductPurchase() { Product = product,ProductQty = pwq.Quantity };

            }).ToList();
            return this.context.SaveChanges() > 0 ? true : false;
        }

        public decimal GetPurchaseQuantity
            (int? purchaseID = null, string supplierName = null, int? supplierID = null,
            DateTime? startDate = null, DateTime? endDate = null)
        {
            var query = this.context.Purchases
                .Include(x => x.ProductsТоPurchase)
                .ThenInclude(x => x.Product).AsQueryable();

            if (purchaseID != null) query = query.Where(x => x.PurchaseID == purchaseID);

            if (supplierName != null)
            {
                query = query.Include(x => x.Supplier);
                query = query.Where(x => x.Supplier.Name == supplierName);
            }

            if (supplierID != null)
            {
                query = query.Include(x => x.Supplier);
                query = query.Where(x => x.Supplier.SupplierID == supplierID);
            }

            if (startDate != null && endDate != null)
                query = query.Where(x => x.PurchaseDate >= startDate && x.PurchaseDate <= endDate);

            return query
                .SelectMany(x => x.ProductsТоPurchase)
                .Select(x => x.ProductQty * x.Product.RetailPrice)
                .Sum();
        }
    }
}
