﻿using Autofac;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Presentation.Menu.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace StoreSystem.Presentation.Menu
{
    public class MainMenuCommands
    {
        public static IList<IMenuItem> GetCommandsList()
        {
            var types = Assembly.GetExecutingAssembly()
                .DefinedTypes
                .Where(t => t.ImplementedInterfaces.Contains(typeof(IEngineCommand)));

            var res = new List<IMenuItem>();

            foreach (var t in types)
            {
                var attr = t.GetCustomAttributes(true).FirstOrDefault() as InfoAttribute;
                if (attr != null)
                {
                    res.Add(new MenuItem
                        (t.Name.ToLower().Replace("command", ""),
                        attr.MenuText,
                        attr.MenuParams));
                }
            }
            res = res.OrderBy(x => x.MenuText).ToList();
            res.Insert(0,new MenuItem(" ", "Select Commands:", " "));
            res.Add(new MenuItem("", "****Exit****", ""));
            return res;
        }

    }
}
