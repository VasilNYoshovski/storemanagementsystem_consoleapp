﻿using Autofac.Core;
using StoreSystem.Presentation.Menu.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Menu
{
    internal static class MainMenuItems
    {
 
        internal static IList<IMenuItem> InputTypeItems = new List<IMenuItem>()
        {
            new MenuItem("","Select input type:",""),
            new MenuItem("MenuCommands","* Use list with commands",""),
            new MenuItem("BatchCommands","* Enter multiple commands",""),
            new MenuItem("AppExit","****Exit****","")
        };



    }

}
