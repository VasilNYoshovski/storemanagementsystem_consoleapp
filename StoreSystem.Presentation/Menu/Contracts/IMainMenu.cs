﻿using System.Collections.Generic;
using StoreSystem.Core.Contracts;

namespace StoreSystem.Presentation.Menu.Contracts
{
    public interface IMainMenu
    {
        //IList<IMenuItem> MenuCommandsList { get; set; }

        void ShowCredits();

        void ShowLogo();

        string ShowMenu(IList<IMenuItem> mainMenuItems);

        //void Start();

        //void ConsoleBatchCommands();

        string ConsoleParameters(int indexOfItem, IList<IMenuItem> mainMenuItems);

        IReader InputTypeChooser();
    }
}