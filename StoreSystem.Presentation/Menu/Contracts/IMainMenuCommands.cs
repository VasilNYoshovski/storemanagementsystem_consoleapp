﻿using System.Collections.Generic;

namespace StoreSystem.Presentation.Menu.Contracts
{
    public interface IMainMenuCommands
    {
        IList<IMenuItem> GetCommandsList();
    }
}