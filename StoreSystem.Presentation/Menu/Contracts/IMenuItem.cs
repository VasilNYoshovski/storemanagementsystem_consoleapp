﻿namespace StoreSystem.Presentation.Menu.Contracts
{
    public interface IMenuItem
    {
        string CommandText { get; set; }
        string MenuText { get; set; }
        string ParamsText { get; set; }
    }
}