﻿using Autofac;
using StoreSystem.Core;
using StoreSystem.Presentation.ContainerModules;

namespace StoreSystem.Data
{
    class StartUp
    {
        private static void Main()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new MainModule());
            builder.RegisterModule(new CommandsModule());
            builder.RegisterModule(new ServicesModule());


            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope())
            {
                var engine = scope.Resolve<Engine>();
                engine.Start();
            }
        }
    }
}
