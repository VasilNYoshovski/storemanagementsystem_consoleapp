﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Autofac;
using StoreSystem.Services;
using StoreSystem.Services.Providers;

namespace StoreSystem.Presentation.ContainerModules
{
    public class ServicesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var servicesAssembly = Assembly.GetAssembly(typeof(SaleService));

            builder.RegisterAssemblyTypes(servicesAssembly)
                   .Where(t => t.Name.EndsWith("Service"))
                   .AsImplementedInterfaces();

            builder.RegisterType<DateTimeNowProvider>().As<IDateTimeNowProvider>();
        }
    }
}
