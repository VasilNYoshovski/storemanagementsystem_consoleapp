﻿using Autofac;
using StoreSystem.Commands.Utils;
using StoreSystem.Core;
using StoreSystem.Core.Contracts;
using StoreSystem.Core.Factories;
using StoreSystem.Core.Factories.Contracts;
using StoreSystem.Core.Utils;
using StoreSystem.Core.Utils.Contracts;
using StoreSystem.Data.DbContext;
using StoreSystem.Presentation.Core.Utils;
using StoreSystem.Presentation.Menu;
using StoreSystem.Presentation.Menu.Contracts;

namespace StoreSystem.Presentation.ContainerModules
{
    class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(LifetimeScopeResolver<>)).As(typeof(IResolver<>));
            builder.RegisterType<StoreSystemDbContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<ComponentsFactory>().As<IComponentsFactory>().SingleInstance();
            builder.RegisterType<MainMenu>().As<IMainMenu>().SingleInstance();
            builder.RegisterType<PrintReports>().As<IPrintReports>().SingleInstance();
            builder.RegisterType<PDFReportWriter>().As<IPDFReportWriter>().SingleInstance();
            builder.RegisterType<ConsoleWriter>().As<IWriter>().SingleInstance();
            builder.RegisterType<CommandProcessor>().As<ICommandProcessor>().SingleInstance();
            builder.RegisterType<MenuReader>().As<IReader>().SingleInstance();
            builder.RegisterType<Engine>().AsSelf();
        }
    }
}
