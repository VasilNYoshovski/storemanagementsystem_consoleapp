﻿using Autofac;
using StoreSystem.Presentation.Commands.Contracts;
using System.Linq;
using System.Reflection;

namespace StoreSystem.Presentation.ContainerModules
{
    public class CommandsModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var commands = Assembly.GetExecutingAssembly()
                    .DefinedTypes
                    .Where(t => t.ImplementedInterfaces.Contains(typeof(IEngineCommand)));

            foreach (var command in commands)
            {
                builder.RegisterType(command.AsType())
                    .Named<IEngineCommand>(command.Name.ToLower().Replace("command", ""));
            }
        }
    }
}
