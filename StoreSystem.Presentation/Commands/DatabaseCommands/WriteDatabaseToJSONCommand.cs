﻿using StoreSystem.Core.Contracts;
using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Presentation.Core.Utils;
using StoreSystem.Services;
using StoreSystem.Services.DatabaseServices;
using System;
using System.Collections.Generic;
using System.IO;

namespace StoreSystem.Presentation.Commands.DatabaseCommands
{
    [Info("* Write database data to JSON archive", "")]
    internal class WriteDatabaseToJSONCommand : IEngineCommand
    {
        private readonly IDatabaseService databaseService;
        private readonly IWriter ConsoleWriter;

        public WriteDatabaseToJSONCommand(IDatabaseService databaseService, IWriter consoleWriter)
        {
            this.databaseService = databaseService ?? throw new ArgumentNullException(nameof(databaseService));
            this.ConsoleWriter = consoleWriter;
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            this.ConsoleWriter.Write($"Start writing database data to JSON.{Environment.NewLine}This may take some time ... {Environment.NewLine}Press any key to continue");

            string jName;
            const string JSON_PATH = "..\\..\\..\\..\\DatabaseArchiveInJSON";
            string jPath = JSON_PATH;
            // Check if the path exists and create it if it does not exists.
            if (string.IsNullOrEmpty(jPath))
            {
                jPath = JSON_PATH;
            }
            else
            {
                jPath = jPath.Trim();
                if (string.IsNullOrEmpty(jPath))
                {
                    jPath = JSON_PATH;
                }
                else
                {
                    jPath = jPath.Replace(" ", "_");
                    if (!Directory.Exists(jPath))
                    {
                        try
                        {
                            Directory.CreateDirectory(jPath);
                        }
                        catch (IOException ex)
                        {
                            return $"Path does not exist and could not be created! For more details see: {ex.Message}";
                        }
                    }
                }
            }

            // write list of objects to JSON file
            jName = "Addresses.json";
            var tmpList1 = this.databaseService.GetAddresses();
            (new DatabaseJSONConnectivity<Address>()).WriteJSON(tmpList1, jPath, jName);

            jName = "Cities.json";
            var tmpList2 = this.databaseService.GetCities();
            (new DatabaseJSONConnectivity<City>()).WriteJSON(tmpList2, jPath, jName);

            jName = "Clients.json";
            var tmpList3 = this.databaseService.GetClients();
            (new DatabaseJSONConnectivity<Client>()).WriteJSON(tmpList3, jPath, jName);

            jName = "Countries.json";
            var tmpList4 = this.databaseService.GetCountries();
            (new DatabaseJSONConnectivity<Country>()).WriteJSON(tmpList4, jPath, jName);

            jName = "Offers.json";
            var tmpList5 = this.databaseService.GetOffers();
            (new DatabaseJSONConnectivity<Offer>()).WriteJSON(tmpList5, jPath, jName);

            jName = "Products.json";
            var tmpList6 = this.databaseService.GetProducts();
            (new DatabaseJSONConnectivity<Product>()).WriteJSON(tmpList6, jPath, jName);

            jName = "ProductOfOffers.json";
            var tmpList7 = this.databaseService.GetProductOffer();
            (new DatabaseJSONConnectivity<ProductOffer>()).WriteJSON(tmpList7, jPath, jName);

            jName = "ProducPurchases.json";
            var tmpList8 = this.databaseService.GetProductPurchase();
            (new DatabaseJSONConnectivity<ProductPurchase>()).WriteJSON(tmpList8, jPath, jName);

            jName = "ProductSales.json";
            var tmpList9 = this.databaseService.GetProductSale();
            (new DatabaseJSONConnectivity<ProductSale>()).WriteJSON(tmpList9, jPath, jName);

            jName = "ProductSuppliers.json";
            var tmpList10 = this.databaseService.GetProductSupplier();
            (new DatabaseJSONConnectivity<ProductSupplier>()).WriteJSON(tmpList10, jPath, jName);

            jName = "Purchases.json";
            var tmpList11 = this.databaseService.GetPurchases();
            (new DatabaseJSONConnectivity<Purchase>()).WriteJSON(tmpList11, jPath, jName);

            jName = "Sales.json";
            var tmpList12 = this.databaseService.GetSales();
            (new DatabaseJSONConnectivity<Sale>()).WriteJSON(tmpList12, jPath, jName);

            jName = "Suppliers.json";
            var tmpList13 = this.databaseService.GetSupplies();
            (new DatabaseJSONConnectivity<Supplier>()).WriteJSON(tmpList13, jPath, jName);

            jName = "Warehouses.json";
            var tmpList14 = this.databaseService.GetWarehouses();
            (new DatabaseJSONConnectivity<Warehouse>()).WriteJSON(tmpList14, jPath, jName);

            this.ConsoleWriter.Write($"Finished writing database data to JSON!{Environment.NewLine}Press any key to continue");
            return "Database has been written to JSON.";
        }
    }
}
