﻿using StoreSystem.Core.Contracts;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Presentation.Core.Utils;
using StoreSystem.Services;
using StoreSystem.Services.DatabaseServices;
using StoreSystem.Services.Providers;
using System;
using System.Collections.Generic;

namespace StoreSystem.Presentation.Commands.DatabaseCommands
{
    [Info("* Populate database with random data automaticaly", "")]
    class AutoPopulateDatabaseCommand : IEngineCommand
    {
        private readonly IDatabaseService databaseService;
        private readonly IDateTimeNowProvider DateTimeNow;
        private readonly IWriter ConsoleWriter;

        public AutoPopulateDatabaseCommand(IDatabaseService databaseService, IDateTimeNowProvider dateTimeNow, IWriter consoleWriter)
        {
            this.databaseService = databaseService ?? throw new ArgumentNullException(nameof(databaseService));
            this.DateTimeNow = dateTimeNow;
            this.ConsoleWriter = consoleWriter;
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            this.ConsoleWriter.Write($"Starting autiomatic database population.{Environment.NewLine}This may take some time ... {Environment.NewLine}Press any key to continue");
            this.databaseService.DatabaseAutoPopulate();
            this.ConsoleWriter.Write($"Autiomatic database population finished!{Environment.NewLine}Press any key to continue");
            return "Database has been populated with random data automaticaly.";
        }
    }
}
