﻿using StoreSystem.Core.Contracts;
using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.DatabaseServices;
using System;
using System.Collections.Generic;

namespace StoreSystem.Presentation.Commands.DatabaseCommands
{
    [Info("* Populate database data from JSON archive", "")]
    internal class PopulateDatabaseFromJSONCommand : IEngineCommand
    {
        private readonly IDatabaseService databaseService;
        private readonly IWriter ConsoleWriter;

        public PopulateDatabaseFromJSONCommand(DatabaseService databaseService, IWriter consoleWriter)
        {
            this.databaseService = databaseService ?? throw new ArgumentNullException(nameof(databaseService));
            this.ConsoleWriter = consoleWriter;
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            this.ConsoleWriter.Write($"Starting database data population from JSON.{Environment.NewLine}This may take some time ... {Environment.NewLine}Press any key to continue");

            string jName;
            string jPath = "..\\..\\..\\..\\DatabaseArchiveInJSON";

            // read list of objects from JSON file
            jName = "Addresses.json";
            var tmpList1 = (new DatabaseJSONConnectivity<Address>()).ReadJSON(jPath, jName);
            this.databaseService.SetAddresses(tmpList1);

            jName = "Cities.json";
            var tmpList2 = (new DatabaseJSONConnectivity<City>()).ReadJSON(jPath, jName);
            this.databaseService.SetCities(tmpList2);

            jName = "Clients.json";
            var tmpList3 = (new DatabaseJSONConnectivity<Client>()).ReadJSON(jPath, jName);
            this.databaseService.SetClients(tmpList3);

            jName = "Countries.json";
            var tmpList4 = (new DatabaseJSONConnectivity<Country>()).ReadJSON(jPath, jName);
            this.databaseService.SetCountries(tmpList4);

            jName = "Offers.json";
            var tmpList5 = (new DatabaseJSONConnectivity<Offer>()).ReadJSON(jPath, jName);
            this.databaseService.SetOffers(tmpList5);

            jName = "Products.json";
            var tmpList6 = (new DatabaseJSONConnectivity<Product>()).ReadJSON(jPath, jName);
            this.databaseService.SetProducts(tmpList6);

            jName = "ProductOfOffers.json";
            var tmpList7 = (new DatabaseJSONConnectivity<ProductOffer>()).ReadJSON(jPath, jName);
            this.databaseService.SetProductOffer(tmpList7);

            jName = "ProducPurchases.json";
            var tmpList8 = (new DatabaseJSONConnectivity<ProductPurchase>()).ReadJSON(jPath, jName);
            this.databaseService.SetProductPurchase(tmpList8);

            jName = "ProductSales.json";
            var tmpList9 = (new DatabaseJSONConnectivity<ProductSale>()).ReadJSON(jPath, jName);
            this.databaseService.SetProductSale(tmpList9);

            jName = "ProductSuppliers.json";
            var tmpList10 = (new DatabaseJSONConnectivity<ProductSupplier>()).ReadJSON(jPath, jName);
            this.databaseService.SetProductSupplier(tmpList10);

            jName = "Purchases.json";
            var tmpList11 = (new DatabaseJSONConnectivity<Purchase>()).ReadJSON(jPath, jName);
            this.databaseService.SetPurchases(tmpList11);

            jName = "Sales.json";
            var tmpList12 = (new DatabaseJSONConnectivity<Sale>()).ReadJSON(jPath, jName);
            this.databaseService.SetSales(tmpList12);

            jName = "Suppliers.json";
            var tmpList13 = (new DatabaseJSONConnectivity<Supplier>()).ReadJSON(jPath, jName);
            this.databaseService.SetSupplies(tmpList13);

            jName = "Warehouses.json";
            var tmpList14 = (new DatabaseJSONConnectivity<Warehouse>()).ReadJSON(jPath, jName);
            this.databaseService.SetWarehouses(tmpList14);

            this.databaseService.SaveChanges();

            this.ConsoleWriter.Write($"Database population from JSON finished!{Environment.NewLine}Press any key to continue");
            return "Database has been populated from JSON.";
        }
    }
}
