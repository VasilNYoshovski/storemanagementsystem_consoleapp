﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.Utils
{
    public class InfoAttribute : Attribute
    {
        public InfoAttribute(string menuText, string menuParams)
        {
            this.MenuText = menuText;
            this.MenuParams = menuParams;
            //this.PdfDo = pdfDo;
        }

        public string MenuText { get; set; }
        public string MenuParams { get; set; }
       // public string PdfDo { get; }
    }
}
