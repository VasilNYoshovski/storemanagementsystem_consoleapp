﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.Utils
{
    internal static class TotalCounting
    {
        public static string TotalVATString(decimal total)
        {
            var res = new StringBuilder();

            res.Append("           Total: ").AppendLine(total.ToString("f2"));
            res.Append("         VAT 20%: ").AppendLine((total * 0.2m).ToString("f2"));
            res.Append("  Total with VAT: ").AppendLine((total * 1.2m).ToString("f2"));

            return res.ToString();
        }
    }
}
