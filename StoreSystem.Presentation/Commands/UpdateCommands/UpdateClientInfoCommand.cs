﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Modify client information", "Enter client ID, name, UIN, email, phone, address, city, country (to not modify field use \"-\"):")]
    internal class UpdateClientInfoCommand : IEngineCommand
    {
        private readonly IClientService clientService;
        private readonly IAddressService addressService;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public UpdateClientInfoCommand(
            IClientService clientService,
            IAddressService addressService,
            ICityService cityService,
            ICountryService countryService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.addressService = addressService ?? throw new ArgumentNullException(nameof(addressService));
            this.cityService = cityService ?? throw new ArgumentNullException(nameof(cityService));
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 8)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var clientID = int.Parse(parameters[0]);
            var clientName = parameters[1];
            var uin = parameters[2];
            var email = parameters[3];
            var phone = parameters[4];

            var addressText = parameters[5];
            var cityName = parameters[6];
            var countryName = parameters[7];

            var client = this.clientService.FindClientByID(clientID);

            Address address = null;
            if (addressText != Consts.NON_MODIFY)
            {
                address = this.addressService.FindAddresByID(client.AddressID) ?? throw new ArgumentException($"Can not find client previous address");

                if (address != null)
                {
                    address.Name = addressText;
                }
            }

            City city = null;
            if (cityName != Consts.NON_MODIFY)
            {
                city = this.cityService.FindCityByID(client.CityID) ?? throw new ArgumentException($"Can not find client previous city");

                if (city != null)
                {
                    city.Name = cityName;
                }
            }

            Country country = null;
            if (countryName != Consts.NON_MODIFY)
            {
                country = this.countryService.FindCountryByID(client.CountryID) ?? throw new ArgumentException($"Can not find client previous country");

                if (country != null)
                {
                    country.Name = countryName;
                }
            }

            var isExecuted = this.clientService.UpdateClient(clientID, clientName, uin, email, phone, address, city, country);

            return $"Client {client.Name} was updateted!";
        }
    }
}