﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.UpdateCommands
{

    [Info("* Update product details", "Enter product ID, name, measure, quantity, buy price, retail price (to not modify field use \"-\")")]
    internal class UpdateProductDetailsCommand : IEngineCommand
    {
        private readonly IProductService productService;

        public UpdateProductDetailsCommand(IProductService productService)
        {
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var productID = int.Parse(parameters[0]);
            var name = (parameters[1] != Consts.NON_MODIFY) ? parameters[1] : null;
            var measure = (parameters[2] != Consts.NON_MODIFY) ? parameters[2] : null;
            decimal? quantity = (parameters[3] != Consts.NON_MODIFY) ? (decimal?)decimal.Parse(parameters[3]) : null;
            decimal? buyPrice = (parameters[4] != Consts.NON_MODIFY) ? (decimal?)decimal.Parse(parameters[4]) : null;
            decimal? retailPrice = (parameters[5] != Consts.NON_MODIFY) ? (decimal?)decimal.Parse(parameters[5]) : null;

            var isExecuted = this.productService.UpdateProductDetail(productID, name, measure, quantity, buyPrice, retailPrice);

            return (isExecuted) ? $"Product details was updated!" : "Error updating product details!";
        }
    }
}
