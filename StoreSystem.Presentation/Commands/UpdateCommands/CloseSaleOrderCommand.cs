﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.UpdateCommands
{

    [Info("* Close sale (mark as delivered)", "Enter sale ID and delivery date")]
    class CloseSaleOrderCommand : IEngineCommand
    {
        readonly ISaleService saleService;

        public CloseSaleOrderCommand(ISaleService saleService)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 2)
            {
                throw new ArgumentException("Not enough parameters");
            }
            var saleID = int.Parse(parameters[0]);
            var deliveryDate = DateTime.Parse(parameters[1]);
            var isExecuted = saleService.UpdateSaleDeliveryDate(saleID, deliveryDate);
            return (isExecuted) ?
                $"Sale with ID {saleID} was delivered on {deliveryDate:d}. Sale was closed!" :
                "Error update delivery date fiels!";
        }
    }
}
