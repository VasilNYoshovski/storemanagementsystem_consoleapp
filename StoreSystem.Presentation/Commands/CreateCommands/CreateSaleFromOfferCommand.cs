﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create sale from offer", "Enter offer ID:")]
    internal class CreateSaleFromOfferCommand : IEngineCommand
    {
        private readonly IOfferService offerService;
        private readonly ISaleService saleService;
        private readonly IProductOfferService productOfferService;

        public CreateSaleFromOfferCommand(
            IOfferService offerService,
            ISaleService saleService,
            IProductOfferService productOfferService)
        {
            this.offerService = offerService ?? throw new ArgumentNullException(nameof(offerService));
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
            this.productOfferService = productOfferService ?? throw new ArgumentNullException(nameof(productOfferService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 1)
            {
                throw new ArgumentException("Not enough parameters");
            }
            var offerID = int.Parse(parameters[0]);
            var offer = this.offerService.GetOfferByID(offerID);
            if (offer.SaleID != null)
            {
                throw new ArgumentException("Error! This offer has already generated sale.");
            }
            var sale = saleService.CreateSale(
                offer.ClientID,
                offer.ProductDiscount,
                (offer.ExpiredDate - offer.OfferDate).Days,
                offer.AddressID,
                offer.CityID,
                offer.CountryID,
                offer.OfferID
                );

            var productsWithQuantity = productOfferService.GetProductSalesByOfferID(offerID);

            saleService.AddProductsByIdToSale(sale.SaleID, productsWithQuantity.ToArray());
            return $"Sale with ID {sale.SaleID} was created. All products from offer are added to sale.";
        }
    }
}
