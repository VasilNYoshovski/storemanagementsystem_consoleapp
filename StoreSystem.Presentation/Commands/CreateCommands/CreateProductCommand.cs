﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create new product", "Enter product name, measure, quantity, buy pric, retail price")]
    class CreateProductCommand : IEngineCommand
    {
        readonly IProductService productService;

        public CreateProductCommand(IProductService productService)
        {
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var name = parameters[0];
            var measure = parameters[1];
            var quantity = decimal.Parse(parameters[2]);
            var buyPrice = decimal.Parse(parameters[3]);
            var retailPrice = decimal.Parse(parameters[4]);
            var product = productService.CreateProduct(name, measure, quantity, buyPrice, retailPrice);
            return $"Product {product.Name} with ID {product.ProductID} was created.";
        }
    }
}
