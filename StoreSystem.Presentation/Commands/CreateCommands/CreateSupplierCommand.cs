﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create new supplier", "Enter supplier name, UIN, Email, Phone, Address, City, Country:")]
    class CreateSupplierCommand : IEngineCommand
    {
        private readonly ISupplierService supplierService;
        private readonly IAddressService addressService;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public CreateSupplierCommand(
            ISupplierService supplierService,
            IAddressService addressService,
            ICityService cityService,
            ICountryService countryService)
        {
            this.supplierService = supplierService ?? throw new ArgumentNullException(nameof(this.supplierService));
            this.addressService = addressService ?? throw new ArgumentNullException(nameof(this.addressService));
            this.cityService = cityService ?? throw new ArgumentNullException(nameof(this.cityService));
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(this.countryService));
        }
        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 7)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var supplierName = parameters[0];
            var uin = parameters[1];
            var email = parameters[2];
            var phone = parameters[3];

            var addressText = parameters[4];
            var cityName = parameters[5];
            var countryName = parameters[6];

            if (null != this.supplierService.FindSupplierByName(supplierName))
            {
                throw new ArgumentException($"Supplier with name {supplierName} already exists!");
            }

            Country country = this.countryService.FindCountryByName(countryName);
            if (country == null) { country = this.countryService.CreateCountry(countryName, false); }

            City city = this.cityService.FindCityByName(cityName);
            if (city == null) { city = this.cityService.CreateCity(cityName, false); }

            Address address = this.addressService.FindAddressByName(addressText);
            if (address == null) { address = this.addressService.CreateAddress(addressText, false); }

            var supplier = this.supplierService.CreateSupplier(supplierName, uin, email, phone, country.CountryID, city.CityID, address.AddressID);

            return $"Supplier {supplier.Name} with ID {supplier.SupplierID} was created.";

        }
    }
}
