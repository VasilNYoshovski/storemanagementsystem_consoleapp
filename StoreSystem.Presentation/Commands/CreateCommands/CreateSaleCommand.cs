﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;


namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create new sale", "Add sale client name, discount, days to delivery, true or false for client default address, delivery addres [NR], city [NR], country [NR], offer ID [NR]")]
    public class CreateSaleCommand : IEngineCommand
    {
        private readonly ISaleService saleService;
        private readonly IClientService clientService;
        private readonly IAddressService addressService;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public CreateSaleCommand(
            ISaleService saleService,
            IClientService clientService,
            IAddressService addressService,
            ICityService cityService,
            ICountryService countryService)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.addressService = addressService ?? throw new ArgumentNullException(nameof(addressService));
            this.cityService = cityService ?? throw new ArgumentNullException(nameof(cityService));
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));
        }
        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count<4)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var clientName = parameters[0];
            var discount = decimal.Parse(parameters[1]);
            var daysToDelivery = double.Parse(parameters[2]);
            var useClientAddress = bool.Parse(parameters[3]);

            var addressText = "";
            var cityName = "";
            var countryName = "";

            if (!useClientAddress)
            {
                addressText = parameters[4];
                cityName = parameters[5];
                countryName = parameters[6];
            }

            int? offerID = null;
            if (!useClientAddress && parameters.Count == 8)
            {
                offerID = int.Parse(parameters[7]);
            }

            if (useClientAddress && parameters.Count == 5)
            {
                offerID = int.Parse(parameters[4]);
            }

            Client client;
            Address address;
            City city;
            Country country;

            if (useClientAddress)
            {
                client = this.clientService.FindClientWithAddress(clientName);
                address = client.Address;
                city = client.City;
                country = client.Country;
            }
            else
            {
                client = this.clientService.FindClientByName(clientName);

                country = this.countryService.FindCountryByName(countryName);
                if (country == null) country = this.countryService.CreateCountry(countryName, false);

                city = this.cityService.FindCityByName(cityName);
                if (city == null) city = this.cityService.CreateCity(cityName, false);

                address = this.addressService.FindAddressByName(addressText);
                if (address == null) this.addressService.CreateAddress(addressText, false);
            }

            if (client == null)
            {
                throw new ArgumentException($"Client with name {clientName} does not exests!");
            }

            var sale = this.saleService.CreateSale(client, discount, daysToDelivery, address, city, country, offerID);

            return $"Sale with ID {sale.SaleID} was created. Now you can add products to this sale.";
            
        }
    }
}
