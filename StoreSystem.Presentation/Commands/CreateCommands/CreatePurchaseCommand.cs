﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create new purchase", "Add purchase supplier name, days to delivery, Warehouse name")]
    class CreatePurchaseCommand : IEngineCommand
    {
        private readonly IPurchaseService purchaseService;
        private readonly ISupplierService supplierService;
        private readonly IWarehouseService warehauseService;

        public CreatePurchaseCommand(
            IPurchaseService purchaseService,
            ISupplierService supplierService,
            IWarehouseService warehauseService)
        {
            this.purchaseService = purchaseService ?? throw new ArgumentNullException(nameof(this.purchaseService));
            this.supplierService = supplierService ?? throw new ArgumentNullException(nameof(this.supplierService));
            this.warehauseService = warehauseService ?? throw new ArgumentNullException(nameof(this.warehauseService));
        }
        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 3)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var supplierName = parameters[0];
            var daysToDelivery = double.Parse(parameters[1]);
            var warehouseName = parameters[2];

            Supplier supplier = this.supplierService.GetSupplierByName(supplierName);

            Warehouse warehouse = this.warehauseService.GetWarehouseByName(warehouseName);

            var purchase = this.purchaseService.CreatePurchase(supplier.SupplierID, warehouse.WarehouseID, daysToDelivery);

            return $"Purchase with ID {purchase.PurchaseID} was created. Now you can add products to this purchase.";

        }
    }
}
