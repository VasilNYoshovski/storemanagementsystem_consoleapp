﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create new client", "Enter client name, UIN, email, phone, address, city, country:")]
    internal class CreateClientCommand : IEngineCommand
    {
        private readonly IClientService clientService;
        private readonly IAddressService addressService;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public CreateClientCommand(
            IClientService clientService,
            IAddressService addressService,
            ICityService cityService,
            ICountryService countryService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.addressService = addressService ?? throw new ArgumentNullException(nameof(addressService));
            this.cityService = cityService ?? throw new ArgumentNullException(nameof(cityService));
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));
        }
        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 7)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var clientName = parameters[0];
            var uin = parameters[1];
            var email = parameters[2];
            var phone = parameters[3];

            var addressText = parameters[4];
            var cityName = parameters[5];
            var countryName = parameters[6];

            Address address;
            City city;
            Country country;

            country = this.countryService.FindCountryByName(countryName);
            if (country == null) country = this.countryService.CreateCountry(countryName, false);

            city = this.cityService.FindCityByName(cityName);
            if (city == null) city = this.cityService.CreateCity(cityName, false);

            address = this.addressService.FindAddressByName(addressText);
            if (address == null) address = this.addressService.CreateAddress(addressText, false);

            var client = this.clientService.CreateClient(clientName, uin, email, phone, address, city, country);

            return $"Client {client.Name} with {client.ClientID} was created.";

        }
    }
}
