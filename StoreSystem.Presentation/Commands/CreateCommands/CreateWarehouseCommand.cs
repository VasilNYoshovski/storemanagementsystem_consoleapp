﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Create new warehouse", "Enter warehouse name, address, city, country:")]
    class CreateWarehouseCommand : IEngineCommand
    {
        private readonly IWarehouseService warehouseService;
        private readonly IAddressService addressService;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public CreateWarehouseCommand(
            IWarehouseService warehouseService,
            IAddressService addressService,
            ICityService cityService,
            ICountryService countryService)
        {
            this.warehouseService = warehouseService ?? throw new ArgumentNullException(nameof(this.warehouseService));
            this.addressService = addressService ?? throw new ArgumentNullException(nameof(addressService));
            this.cityService = cityService ?? throw new ArgumentNullException(nameof(cityService));
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));
        }
        public string Execute(IReadOnlyList<string> parameters)
        {
            //Console.WriteLine("***** " + StoreSystemDbContext.count);

            if (parameters.Count != 4)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var warehouseName = parameters[0];
            var addressText = parameters[1];
            var cityName = parameters[2];
            var countryName = parameters[3];

            Country country = this.countryService.FindCountryByName(countryName);
            if (country == null) { country = this.countryService.CreateCountry(countryName, false); }

            City city = this.cityService.FindCityByName(cityName);
            if (city == null) { city = this.cityService.CreateCity(cityName, false); }

            Address address = this.addressService.FindAddressByName(addressText);
            if (address == null) { address = this.addressService.CreateAddress(addressText, false); }

            var warehouse = this.warehouseService.CreateWarehouse(warehouseName, country.CountryID, city.CityID, address.AddressID);

            return $"Warehouse {warehouse.Name} with ID {warehouse.WarehouseID} was created.";

        }
    }
}
