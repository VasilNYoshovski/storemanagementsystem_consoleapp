﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.Dto;
using System;
using System.Collections.Generic;

namespace StoreSystem.Presentation.Commands.AddCommands
{
    [Info("* Add products to purchase", "Enter purchaseID product_name1 quantity1 product_name2 quantity2 ...")]
    class AddProductsToPurchaseCommand : IEngineCommand
    {
        private readonly IPurchaseService purchaseService;
        private readonly IProductService productService;

        public AddProductsToPurchaseCommand(IPurchaseService purchaseService, IProductService productService)
        {
            this.purchaseService = purchaseService ?? throw new ArgumentNullException(nameof(purchaseService));
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var purchaseID = int.Parse(parameters[0]);

            var productsWithQuantity = new List<ProductQuantityDto>();
            for (int i = 1; i < parameters.Count; i += 2)
            {
                productsWithQuantity.Add(new ProductQuantityDto(parameters[i], decimal.Parse(parameters[i + 1])));
            }
            var isExecuted = purchaseService.AddProductsToPurchase(purchaseID, productsWithQuantity.ToArray());

            return isExecuted ? $"{(parameters.Count - 1) / 2} products was added to purchase ID: {purchaseID}." : "Command was not executed!";
        }
    }
}
