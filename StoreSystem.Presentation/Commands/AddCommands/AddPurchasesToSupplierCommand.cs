﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Presentation.Commands.AddCommands
{
    //[Info("* Add purchases to supplier", "Enter supplierID purchaseID1 purchaseID2 ........ purchaseIDx")]
    class AddPurchasesToSupplierCommand : IEngineCommand
    {
        private readonly ISupplierService supplierService;
        private readonly IProductService productService;

        public AddPurchasesToSupplierCommand(ISupplierService supplierService, IProductService productService)
        {
            this.supplierService = supplierService ?? throw new ArgumentNullException(nameof(this.supplierService));
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 2)
            {
                throw new ArgumentException("Not enough parameters");
            }
            var supplierID = int.Parse(parameters[0]);

            var purchasesList = Array.ConvertAll(parameters.Skip(1).ToArray(), s => int.Parse(s));
            var isExecuted = supplierService.AddPurchasesToSupplier(supplierID, purchasesList);

            return isExecuted ? $"{(parameters.Count - 1) / 2} products was added to purchase ID: {supplierID}." : "Command was not executed!";
        }
    }
}
