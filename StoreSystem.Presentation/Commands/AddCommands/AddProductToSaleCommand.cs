﻿using System;
using System.Collections.Generic;
using System.Linq;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.Dto;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Add products to sale", "Enter sale ID product_name1 quantity1 product_name2 quantity2 ...")]
    public class AddProductsToSale : IEngineCommand
    {
        private readonly ISaleService saleService;
        private readonly IProductService productService;

        public AddProductsToSale(ISaleService saleService, IProductService productService)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var saleID = int.Parse(parameters[0]);

            var productsWithQuantity = new List<ProductQuantityDto>();
            for (int i = 1; i < parameters.Count; i += 2)
            {
                productsWithQuantity.Add(new ProductQuantityDto(parameters[i], decimal.Parse(parameters[i + 1])));
            }
            var isExecuted = saleService.AddProductsToSale(saleID, productsWithQuantity.ToArray());

            return isExecuted ? $"{(parameters.Count - 1) / 2} products was added to sale ID: {saleID}." : "Command was not executed!";
        }
    }
    
}