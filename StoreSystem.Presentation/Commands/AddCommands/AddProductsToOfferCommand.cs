﻿using System;
using System.Collections.Generic;
using System.Linq;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;

namespace StoreSystem.Presentation.Commands.CreateCommands
{
    [Info("* Add products to offer", "Enter offer ID product_name1 quantity1 product_name2 quantity2 ...")]
    public class AddProductsToOffer : IEngineCommand
    {
        private readonly IOfferService offerService;
        private readonly IProductService productService;

        public AddProductsToOffer(IOfferService offerService, IProductService productService)
        {
            this.offerService = offerService ?? throw new ArgumentNullException(nameof(offerService));
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var offerID = int.Parse(parameters[0]);

            var productsWithQuantity = new List<KeyValuePair<string, decimal>>();
            for (int i = 1; i < parameters.Count; i += 2)
            {
                productsWithQuantity.Add(new KeyValuePair<string, decimal>(parameters[i], decimal.Parse(parameters[i + 1])));
            }
            var isExecuted = offerService.AddProductsToOffer(offerID, productsWithQuantity.ToArray());

            return isExecuted ? $"{(parameters.Count - 1) / 2} products was added to offer ID: {offerID}." : "Command was not executed!";
        }
    }

}