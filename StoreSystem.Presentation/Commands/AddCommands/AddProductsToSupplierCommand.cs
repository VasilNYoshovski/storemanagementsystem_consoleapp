﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.Dto;
using System;
using System.Collections.Generic;

namespace StoreSystem.Presentation.Commands.AddCommands
{
    [Info("* Add products to supplier", "Enter supplierID product_name1 product_name2 ...")]
    class AddProductsToSupplierCommand : IEngineCommand
    {
        private readonly ISupplierService supplierService;
        private readonly IProductService productService;

        public AddProductsToSupplierCommand(ISupplierService supplierService, IProductService productService)
        {
            this.supplierService = supplierService ?? throw new ArgumentNullException(nameof(supplierService));
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var supplierID = int.Parse(parameters[0]);

            var productsWithQuantity = new List<string>();
            for (int i = 1; i < parameters.Count; i += 2)
            {
                productsWithQuantity.Add(new String(parameters[i]));
            }
            var isExecuted = supplierService.AddProductsToSupplier(supplierID, productsWithQuantity.ToArray());

            return isExecuted ? $"{(parameters.Count - 1) / 2} products was added to supplier ID: {supplierID}." : "Command was not executed!";
        }
    }
}
