﻿using System.Collections.Generic;

namespace StoreSystem.Presentation.Commands.Contracts
{
    public interface IEngineCommand
    {
        string Execute(IReadOnlyList<string> parameters);
    }
}
