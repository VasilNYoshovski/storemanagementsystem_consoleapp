﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.DeleteCommands
{
    [Info("* Delete sale", "Enter sale ID:")]
    class DeleteSaleCommand : IEngineCommand
    {
        readonly ISaleService saleService;

        public DeleteSaleCommand(ISaleService saleService)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 1)
            {
                throw new ArgumentException("Not enough parameters");
            }
            var saleID = int.Parse(parameters[0]);

            var isExecuted = saleService.DeleteSale(saleID);

            return (isExecuted) ? $"Sale with ID {saleID} was deleted!" : $"Error deleting sale with ID {saleID}!";
        }
    }
}
