﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* List all product in stock", "Enter take and skip records, part of product name [NR]")]
    class ShowAllProductsInStockCommand : IEngineCommand
    {
        private readonly IProductService productService;

        public ShowAllProductsInStockCommand(IProductService productService)
        {
            this.productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 2)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var take = int.Parse(parameters[0]);
            var skip = int.Parse(parameters[1]);
            var allies = "*";
            if (parameters.Count == 3) allies = parameters[2];
            var list = productService.GetAllProducts(skip, take, allies);

            var res = new StringBuilder();
            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show all products in stock {skip}, take {take}";

            res.AppendLine(reportTitle);
            res.AppendLine(string.Join(Environment.NewLine, list));
            return res.ToString();
        }
    }
}
