﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Products total sale quantity", "")]
    internal class ProductsTotalSaleQuantityCommand : IEngineCommand
    {
        private readonly IProductSaleService productSaleService;

        public ProductsTotalSaleQuantityCommand(IProductSaleService productSaleService)
        {
            this.productSaleService = productSaleService ?? throw new ArgumentNullException(nameof(productSaleService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var alias = "*";
            if (parameters.Count()>0)
            {
                alias = parameters[0];
            }

            var salesByProduct = alias != "*" ?
                this.productSaleService.GetProductsTotalSaleQuantity(alias) :
                this.productSaleService.GetProductsTotalSaleQuantity();
            var res = new StringBuilder();
            res.AppendLine("List of total sales by product");
            salesByProduct.ForEach(item =>
            {
                res.Append(" * ").Append(item.First().Product).Append(" - Quantity: ");
                res.Append(item.Sum(x => x.Quantity).ToString("f2"));
                res.Append(" Total: ");
                res.AppendLine(item.Sum(x => x.Total).ToString("f2"));
            });

            return res.ToString();
        }
    }
}
