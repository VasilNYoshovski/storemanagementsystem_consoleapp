﻿using StoreSystem.Data.Models;
using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Show client info", "Enter client name or ID")]
    internal class ShowClientInfoCommand : IEngineCommand
    {

        private readonly IClientService clientService;

        public ShowClientInfoCommand(IClientService clientService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
        }

        string FullClientInfoToString(Client client)
        {
            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show client {client.Name} info";

            res.AppendLine(reportTitle);

            res.AppendLine(client.ToString());
            res.Append("Address: ");
            res.Append(client.Address.Name).Append(", ");
            res.Append(client.City.Name).Append(", ");
            res.AppendLine(client.Country.Name);

            return res.ToString();
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count == 0)
            {
                throw new ArgumentException("Not enough parameters");
            }

            var IsID = int.TryParse(parameters[0], out var IdParam);

            return IsID?
                FullClientInfoToString(this.clientService.FindClientWithAddress(IdParam)):
                FullClientInfoToString(this.clientService.FindClientWithAddress(parameters[0]));
        }
    }
}
