﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* List not closed sales", "")]
    class ShowNotClosedSalesCommand : IEngineCommand
    {
        readonly ISaleService saleService;

        public ShowNotClosedSalesCommand(ISaleService saleService)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var notClosedSales = saleService.GetNotCLosedSales();
            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show not closed sales";

            res.AppendLine(reportTitle);
            res.AppendLine(string.Join(Environment.NewLine,notClosedSales));
            return res.ToString();
        }
    }
}
