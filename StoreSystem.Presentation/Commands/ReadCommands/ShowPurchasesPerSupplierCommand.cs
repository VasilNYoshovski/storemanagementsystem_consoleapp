﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Show supplier purchases", "Enter supplier name or ID")]
    internal class ShowPurchasesPerSupplierCommand : IEngineCommand
    {
        private readonly ISupplierService supplierService;
        private readonly IPurchaseService purchaseService;

        public ShowPurchasesPerSupplierCommand(ISupplierService supplierService, IPurchaseService purchaseService)
        {
            this.supplierService = supplierService ?? throw new ArgumentNullException(nameof(supplierService));
            this.purchaseService = purchaseService ?? throw new ArgumentNullException(nameof(purchaseService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {

            var IsID = int.TryParse(parameters[0], out var supplierID);

            var supplierName = parameters[0];

            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show purchases per supplier {supplierName}";

            res.AppendLine(reportTitle);

            res.AppendLine($"List of supplier (supplier ID) {supplierName} purchases:");
            res.AppendLine();

            var purchasesList = IsID ? this.supplierService.GetSupplierPurchases(supplierID) : this.supplierService.GetSupplierPurchases(supplierName);

            if (purchasesList.Count == 0)
            {
                res.AppendLine($"  ***Supplier (supplier ID) {supplierName} has no purchases!");
            }
            else
            {
                res.AppendLine(string.Join(Environment.NewLine, purchasesList));
                var total = IsID ? purchaseService.GetPurchaseQuantity(supplierID: supplierID) : purchaseService.GetPurchaseQuantity(supplierName: supplierName);
                res.AppendLine(TotalCounting.TotalVATString(total));
            }

            return res.ToString();
        }

    }
}
