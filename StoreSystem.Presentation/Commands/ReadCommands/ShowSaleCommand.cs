﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Show sale", "Enter sale ID")]
    internal class ShowSaleCommand : IEngineCommand
    {
        private readonly ISaleService saleService;

        public ShowSaleCommand(ISaleService saleService)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            if (parameters.Count < 1)
            {
                throw new ArgumentException("Not enough parameters");
            }
            var saleID = int.Parse(parameters[0]);
            var sale = this.saleService.GetSaleByID(saleID);
            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle =  $"Sale with ID {saleID}";

            res.AppendLine(reportTitle);

            res.AppendLine(sale.ToString());

            var products = this.saleService.GetAllProductsInSale(saleID);
            if (products.Count == 0)
            {
                res.AppendLine(" *** No products in this sale");
            }
            else
            {
                res.AppendLine("*******List of products********");
                res.AppendLine(string.Join(Environment.NewLine, products.Select(x => x.ToString())));
                var total = saleService.GetSaleQuantity(saleID: saleID);
                res.AppendLine(TotalCounting.TotalVATString(total));
            }


            return res.ToString();
        }
    }
}
