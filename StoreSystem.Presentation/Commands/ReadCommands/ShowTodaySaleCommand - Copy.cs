﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* List of today sales", "")]
    class ShowTodaySaleCommand : IEngineCommand
    {
        private readonly ISaleService saleService;
        private readonly IDateTimeNowProvider dateTimeNowProvider;

        public ShowTodaySaleCommand(ISaleService saleService, IDateTimeNowProvider dateTimeNowProvider)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
            this.dateTimeNowProvider = dateTimeNowProvider ?? throw new ArgumentNullException(nameof(dateTimeNowProvider));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show today sales";

            res.AppendLine(reportTitle);

            res.AppendLine("List of today's sales:");
            var nowDate = dateTimeNowProvider.Now.Date;
            var sales = saleService.GetSalesByDate(nowDate,nowDate.AddDays(1));
            if (sales.Count == 0) res.AppendLine(" ***There is not any sales today.");
            else
            {
                res.AppendLine("   "+string.Join(Environment.NewLine, sales));
                var total = saleService.GetSaleQuantity(startDate: nowDate,endDate: nowDate.AddDays(1));
                res.AppendLine(TotalCounting.TotalVATString(total));

            }
            return res.ToString();
        }
    }
}
