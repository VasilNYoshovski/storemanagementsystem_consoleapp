﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Show client sales", "Enter client name or ID")]
    internal class ShowSalesPerClientCommand : IEngineCommand
    {
        private readonly IClientService clientService;
        private readonly ISaleService saleService;

        public ShowSalesPerClientCommand(IClientService clientService, ISaleService saleService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {

            var IsID = int.TryParse(parameters[0], out var clientID);

            var clientName = parameters[0];

            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show sales per client {clientName}";

            res.AppendLine(reportTitle);

            res.AppendLine($"List of client (client ID) {clientName} sales:");
            res.AppendLine();

            var salesList = IsID ? this.clientService.GetClientSales(clientID) : this.clientService.GetClientSales(clientName);

            if (salesList.Count == 0)
            {
                res.AppendLine($"  ***Client (client ID) {clientName} has no sales!");
            }
            else
            {
                res.AppendLine(string.Join(Environment.NewLine, salesList));
                var total = IsID ? saleService.GetSaleQuantity(clientID: clientID) :saleService.GetSaleQuantity(clientName:clientName);
                res.AppendLine(TotalCounting.TotalVATString(total));
            }

            return res.ToString();
        }
    }
}
