﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using StoreSystem.Services.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* List of this week sales", "")]
    internal class ShowThisWeekSaleCommand : IEngineCommand
    {
        private readonly ISaleService saleService;
        private readonly IDateTimeNowProvider dateTimeNowProvider;

        public ShowThisWeekSaleCommand(ISaleService saleService, IDateTimeNowProvider dateTimeNowProvider)
        {
            this.saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
            this.dateTimeNowProvider = dateTimeNowProvider ?? throw new ArgumentNullException(nameof(dateTimeNowProvider));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var res = new StringBuilder();

            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show this week sales";

            res.AppendLine(reportTitle);

            res.AppendLine("List of this week sales:");
            res.AppendLine();
            var nowDate = this.dateTimeNowProvider.Now.Date;
            var firstWeekDay = nowDate.StartOfWeek(DayOfWeek.Monday);
            var sales = this.saleService.GetSalesByDate(firstWeekDay, nowDate.AddDays(1));
            if (sales.Count == 0) res.AppendLine(" ***There is not any sales this week.");
            else
            {
                res.AppendLine(string.Join(Environment.NewLine, sales));
                var total = this.saleService.GetSaleQuantity(startDate:firstWeekDay,endDate: nowDate.AddDays(1));
                res.AppendLine(TotalCounting.TotalVATString(total));
            }
            return res.ToString();
        }
    }
}
