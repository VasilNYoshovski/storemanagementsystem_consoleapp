﻿using StoreSystem.Presentation.Commands.Contracts;
using StoreSystem.Presentation.Commands.Utils;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoreSystem.Presentation.Commands.ReadCommands
{
    [Info("* Show offer", "Enter offer ID")]
    internal class ShowOfferCommand : IEngineCommand
    {
        private readonly IOfferService offerService;

        public ShowOfferCommand(IOfferService offerService)
        {
            this.offerService = offerService ?? throw new ArgumentNullException(nameof(offerService));
        }

        public string Execute(IReadOnlyList<string> parameters)
        {
            var offerID = int.Parse(parameters[0]);
            var offer = this.offerService.GetOfferWithClientByID(offerID);
            var res = new StringBuilder();
            res.Append(CommandsConsts.pdfDo);
            string reportTitle = $"Show offer ID {offerID}";

            res.AppendLine(reportTitle);
            res.AppendLine(offer.ToString());

            var products = this.offerService.GetAllProductsInOffer(offerID);
            if (products.Count == 0)
            {
                res.AppendLine(" *** No products in this offer");
            }
            else
            {
                res.AppendLine("*******List of products********");
                res.AppendLine(string.Join(Environment.NewLine, products.Select(x => x.ToString())));
                var total = offerService.GetOfferQuantity(offerID);
                res.AppendLine(TotalCounting.TotalVATString(total));
            }

            return res.ToString();
        }
    }
}
