﻿using Autofac;

namespace StoreSystem.Core.Factories
{
    public interface IResolver<T>
    {
        T GetService(string namedService);

        ILifetimeScope BeginLifetimeScope();

        void Dispose();
    }
}