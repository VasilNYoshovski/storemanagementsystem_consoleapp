﻿using System;
using Autofac;

namespace StoreSystem.Core.Factories
{
    public class LifetimeScopeResolver<T> : IResolver<T>, IDisposable
    {
        private readonly ILifetimeScope lifetimeScope;

        public LifetimeScopeResolver(ILifetimeScope lifetimeScope)
        {
            this.lifetimeScope = lifetimeScope ?? throw new ArgumentNullException(nameof(lifetimeScope));
        }

        public void Dispose()
        {
            lifetimeScope.Dispose();
        }

        public T GetService(string namedService)
        {
            return lifetimeScope.ResolveNamed<T>(namedService);
        }

        public ILifetimeScope BeginLifetimeScope()
        {
            return lifetimeScope.BeginLifetimeScope();
        }
    }
}