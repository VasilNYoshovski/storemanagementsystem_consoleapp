﻿using Autofac;
using Autofac.Core.Registration;
using StoreSystem.Core.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using StoreSystem.Presentation.Commands.Contracts;

namespace StoreSystem.Core
{
    public class CommandProcessor : ICommandProcessor
    {
        private readonly IResolver<IEngineCommand> lifetimeScope;

        public CommandProcessor(IResolver<IEngineCommand> lifetimeScope)
        {
            this.lifetimeScope = lifetimeScope;
        }

        private (string name, IReadOnlyList<string> args) Spliter(String input)
        {
            string command; 
            var res = new List<string>();
            var nextChar = " ";
            var emptyS = input.IndexOf(" ", 0);
            if (emptyS == -1) emptyS = input.Length + 1;
            var quoteS = input.IndexOf(" '", 0);
            if (quoteS == -1) quoteS = input.Length + 1;

            if (quoteS == input.Length + 1 && emptyS == input.Length + 1)
            {
                //res.Add(input);
                return (input,new List<string>());
            }
            int cur = 0;
            if (quoteS <= emptyS)
            {
                nextChar = " '";
                cur = quoteS;
            }
            else
            {
                nextChar = " ";
                cur = emptyS;
            }
            //res.Add(input.Substring(0, cur));
            command = input.Substring(0, cur);
            while (true)
            {
                if (nextChar == " '")
                {
                    var next = input.IndexOf("' ", cur + 2);
                    if (next == -1) next = input.Length - 1;
                    res.Add(input.Substring(cur+2, next - cur-2));
                    if (next >= input.Length - 1) break;
                    cur = next + 1;
                }
                else
                {
                    var next = input.IndexOf(" ", cur + 1);
                    if (next == -1) next = input.Length;
                    res.Add(input.Substring(cur+1, next - cur-1));
                    if (next >= input.Length - 1) break;
                    cur = next;
                }
                emptyS = input.IndexOf(" ", cur);
                if (emptyS == -1) emptyS = input.Length + 1;
                quoteS = input.IndexOf(" '", cur);
                if (quoteS == -1) quoteS = input.Length + 1;

                nextChar = quoteS <= emptyS ? " '" : " ";

                if (quoteS == input.Length + 1 && emptyS == input.Length + 1)
                {
                    throw new ArgumentException("Parse error");
                }
            }
            return (command,res);
        }

        public string Process(string input)
        {
            using (var childScope = this.lifetimeScope.BeginLifetimeScope())
            {
                var (name, args) = this.Spliter(input);

                try
                {
                    var command = childScope.ResolveNamed<IEngineCommand>(name);
                    return command.Execute(args);
                }
                catch (ComponentNotRegisteredException ex)
                {
                    return $"Command with name \"{name}\" was not found!";
                }
                catch (ArgumentException ex)
                {
                    return ex.Message;
                }
                catch (Exception ex) when (ex is DbUpdateException)
                {
                    while (ex.InnerException != null)
                        ex = ex.InnerException;

                    return $"Sql error occurred: {ex.Message}";
                }
            }
        }

        private (string name, IReadOnlyList<string> args) ParseInput(string input)
        {
            var parts = input.Split();

            return (parts[0], parts.Skip(1).ToList());
        }
    }
}
