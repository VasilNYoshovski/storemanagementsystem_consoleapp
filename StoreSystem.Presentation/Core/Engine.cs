﻿using System;
using StoreSystem.Commands.Utils;
using StoreSystem.Core.Contracts;
using StoreSystem.Core.Utils.Contracts;
using StoreSystem.Presentation.Core.Utils;
using StoreSystem.Presentation.Menu.Contracts;
using StoreSystem.PresentationCommands.Utils;
using StoreSystem.Utils;

namespace StoreSystem.Core
{
    internal class Engine
    {
        private readonly IPrintReports printReports;
        private readonly IMainMenu mainMenu;
        private readonly IReader reader;
        private readonly IPDFReportWriter pDFReportWriter;
        private readonly ICommandProcessor commandProcessor;

        //public IReader ConsoleReader { get; }

        public Engine(
            IPrintReports printReports,
            IMainMenu mainMenu,
            ICommandProcessor commandProcessor,
            IPDFReportWriter pDFReportWriter,
            IReader reader)
        {
            this.printReports = printReports ?? throw new ArgumentException(string.Format(Consts.NULL_OBJECT, nameof(printReports)));
            this.mainMenu = mainMenu ?? throw new ArgumentException(string.Format(Consts.NULL_OBJECT, nameof(mainMenu)));
            this.reader = reader ?? throw new ArgumentNullException(nameof(reader));
            this.commandProcessor = commandProcessor ?? throw new ArgumentNullException(nameof(commandProcessor));
            this.pDFReportWriter = pDFReportWriter ?? throw new ArgumentNullException(nameof(pDFReportWriter));
        }

        public void Start()
        {
            //mainMenu.MenuCommandsList = menuCommands.GetCommandsList();
        
            this.mainMenu.ShowLogo();

            while (true)
            {

                var myReader = this.mainMenu.InputTypeChooser();
                if (myReader == null)
                {
                    this.mainMenu.ShowCredits();
                    return;
                }
                try
                {
                    var inputStringList = myReader.Read();

                    foreach (var inputString in inputStringList)
                    {
                        if (inputString.ToLower() == CommandsConsts.TerminationAppCommand)
                        {
                            this.mainMenu.ShowCredits();
                            return;
                        }

                        if (inputString == CommandsConsts.ConsoleExitCommand)
                        {
                            this.printReports.Print();
                            break;
                        }

                        var report = this.commandProcessor.Process(inputString);
                        this.printReports.Reports.Add(report);
                    }
                }
                catch (Exception ex)
                {
                    this.printReports.Reports.Add(ex.Message);
                    this.printReports.Print();
                }
            }
        }
    }
}
