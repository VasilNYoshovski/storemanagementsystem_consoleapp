﻿namespace StoreSystem.Core.Contracts
{
    internal interface IWriter
    {
        void Write(string message);
    }
}
