﻿namespace StoreSystem.Core
{
    public interface ICommandProcessor
    {
        string Process(string input);
    }
}