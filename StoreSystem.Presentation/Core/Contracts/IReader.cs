﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Core.Contracts
{
    public interface IReader
    {
        ICollection<string> Read();
    }
}
