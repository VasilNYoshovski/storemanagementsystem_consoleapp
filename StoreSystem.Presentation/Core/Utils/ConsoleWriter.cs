﻿using System;
using StoreSystem.Core.Contracts;

namespace StoreSystem.Core.Utils
{
    internal class ConsoleWriter : IWriter
    {
        public void Write(string message)
        {
            Console.WriteLine(message);
            Console.ReadKey();
        }
    }
}
