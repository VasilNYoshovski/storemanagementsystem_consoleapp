﻿using StoreSystem.Core.Contracts;
using StoreSystem.Core.Utils.Contracts;
using StoreSystem.Presentation.Core.Utils;
using StoreSystem.PresentationCommands.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreSystem.Commands.Utils
{
    internal class PrintReports : IPrintReports
    {
        private readonly IWriter writer;
        private readonly IPDFReportWriter pDFReportWriter;
        private IList<string> reports;

        public PrintReports(IWriter writer, IPDFReportWriter pDFReportWriter)
        {
            this.writer = writer ?? throw new ArgumentNullException(string.Format(CommandsConsts.NULL_OBJECT, nameof(writer)));
            this.pDFReportWriter = pDFReportWriter ?? throw new ArgumentNullException(nameof(pDFReportWriter));
            this.reports = new List<string>();
        }

        public IList<string> Reports
        {
            get
            {
                return this.reports;
            }

            set
            {
                this.reports = value;
            }
        }

        public void Print()
        {
            if (this.Reports.Count != 0)
            {
                var output = new StringBuilder();

                foreach (var report in this.Reports)
                {
                    output.AppendLine(report);
                }
                if (output.ToString().StartsWith(CommandsConsts.pdfDo))
                {
                    output.Remove(0, CommandsConsts.pdfDo.Length);
                    output.AppendLine(this.pDFReportWriter.Write(output.ToString()));
                }
                this.writer.Write(output.ToString());
                this.Reports.Clear();
            }
        }
    }
}
