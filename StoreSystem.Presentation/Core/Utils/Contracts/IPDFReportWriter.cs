﻿namespace StoreSystem.Presentation.Core.Utils
{
    interface IPDFReportWriter
    {
        string Write(string message);
    }
}