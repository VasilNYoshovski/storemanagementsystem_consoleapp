﻿using System.Collections.Generic;

namespace StoreSystem.Core.Utils.Contracts
{
    interface IPrintReports
    {
        void Print();

        IList<string> Reports { get; set; }
    }
}