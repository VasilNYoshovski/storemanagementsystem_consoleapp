﻿using System;
using System.Collections.Generic;
using StoreSystem.Commands.Utils;
using StoreSystem.Core.Contracts;

namespace StoreSystem.Core.Utils
{
    internal class ConsoleReader : IReader
    {
        public ICollection<string> Read()
        {
            var result = new List<string>();
            while (true)
            {
                result.Add(Console.ReadLine());

                if (string.IsNullOrEmpty(result[result.Count-1]))
                {
                    break;
                }
            }
            return result;
        }
    }
}
