﻿using System.Collections.Generic;
using StoreSystem.Commands.Utils;
using StoreSystem.Core.Contracts;
using StoreSystem.Presentation.Menu;
using StoreSystem.Presentation.Menu.Contracts;
using StoreSystem.PresentationCommands.Utils;

namespace StoreSystem.Core.Utils
{
    internal class MenuReader : IReader
    {
        private readonly IMainMenu mainMenu;

        public MenuReader(IMainMenu mainMenu)
        {
            this.mainMenu = mainMenu;
        }
        public ICollection<string> Read()
        {
            return new List<string>()
            {
                this.mainMenu.ShowMenu(MainMenuCommands.GetCommandsList()),
                CommandsConsts.ConsoleExitCommand
            };
        }
    }
}
