﻿using Autofac;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using StoreSystem.Core.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace StoreSystem.Presentation.Core.Utils
{
    class PDFReportWriter : IPDFReportWriter
    {
        private static List<string> GetHeader(string reportType, int charactersPerLine)
        {
            List<string> tmpList = new List<string>();
            tmpList.AddRange(NormalizeRow($"Store Management System", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"{reportType.ToUpper()}", charactersPerLine));
            tmpList.Add(" ");
            return tmpList;
        }

        private static List<string> GetFooter(int charactersPerLine, int pageNumber)
        {
            List<string> tmpList = new List<string>();
            tmpList.Add(" ");
            tmpList.AddRange(NormalizeRow($"Store Management System LTD", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"Addres: Bulgaria, Sofia", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"1 Vitosha str., ent.1, floor 1, app.1", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"email: Info@StoreManagementSystem.com", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"mobile: +359 888 888 888", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"Date: {DateTime.Now}", charactersPerLine));
            tmpList.AddRange(NormalizeRow($"page: {pageNumber}", charactersPerLine));
            return tmpList;
        }

        private static List<string> NormalizeRow(string text, int width)
        {
            List<string> tmpList = new List<string>();
            if (text.Length <= width)
            {
                tmpList.Add(text);
            }
            else
            {
                while (text.Length > 0)
                {
                    var subString = text.Substring(0, width);
                    text = text.Substring(width);
                    tmpList.Add(subString);
                    if (text.Length <= width)
                    {
                        tmpList.Add((string)text);
                        text = "";
                    }
                }
            }
            return tmpList;
        }

        private static List<string> NormalizeRows(string[] textRows, int width)
        {
            List<string> tmpList = new List<string>();
            foreach (var rowtext in textRows)
            {
                tmpList.AddRange(NormalizeRow(rowtext, width));
            }
            return tmpList;
        }

        private static (string fileName, bool isCreated) WriteReportToUniquePDF(
            string textToWrite,
            //string reportType,
            string filePath,
            string fileName,
            int fontSize,
            int textOffsetX,
            int textOffsetY)
        {
            int pagenumber = 0;
            int rowNumber;
            //reportType = reportType.ToUpper();

            // Check if the path exists and create it if it does not exists.
            if (string.IsNullOrEmpty(filePath))
            {
                filePath = "C:\\StoreManagementReports";
            }
            else
            {
                filePath = filePath.Trim();
                if (string.IsNullOrEmpty(filePath))
                {
                    filePath = "C:\\StoreManagementReports";
                }
                else
                {
                    filePath = filePath.Replace(" ", "_");
                    if (!Directory.Exists(filePath))
                    {
                        try
                        {
                            Directory.CreateDirectory(filePath);
                        }
                        catch (IOException ex)
                        {
                            return ($"Path does not exist and could not be created! For more details see: {ex.Message}", false);
                        }
                    }
                }
            }

            // pdf sharp
            // Create a new PDF document
            PdfDocument document = new PdfDocument();
            document.Info.Title = "Created with PDFsharp";

            var textToWriteList = textToWrite.Split(Environment.NewLine);

            var reportType = textToWriteList[0];
            textToWriteList = textToWriteList.Skip(1).ToArray();

            Queue<string> queueString = null;
            do
            {
                pagenumber++;
                // Create an empty page
                PdfPage page = document.AddPage();

                // Get an XGraphics object for drawing
                XGraphics gfx = XGraphics.FromPdfPage(page);

                // Create a font
                XFont font = new XFont("Courier", fontSize, XFontStyle.BoldItalic);

                // Draw the text
                int rowsPerPage = (int)((page.Height - 2 * textOffsetY) / fontSize);
                int charactersPerLine = (int)(((page.Width - 2 * textOffsetX) * 40) / (fontSize * 25));
                var tmpHeader = GetHeader(reportType, charactersPerLine);
                var tmpFooter = GetFooter(charactersPerLine, pagenumber);

                // Print HEADER
                rowNumber = 0;
                foreach (var item in tmpHeader)
                {
                    gfx.DrawString($"{item}", font, XBrushes.LightGray,
                    new XRect(0, textOffsetY + rowNumber * fontSize, page.Width, page.Height),
                    XStringFormats.TopCenter);
                    rowNumber++;
                }

                // Print TEXT OF THE REPORT
                if (queueString == null)
                {
                    queueString = new Queue<string>();
                    foreach (var row in NormalizeRows(textToWriteList, charactersPerLine))
                    {
                        queueString.Enqueue(row);
                    }
                }
                if (queueString == null)
                {
                    queueString = new Queue<string>();
                }
                for (int i = (rowsPerPage - tmpHeader.Count - tmpFooter.Count - 2); i > 0; i--)
                {
                    if (queueString.Count < 1)
                    {
                        break;
                    }
                    gfx.DrawString($"{queueString.Dequeue()}", font, XBrushes.Black,
                    new XRect(textOffsetX, textOffsetY + rowNumber * fontSize, page.Width, page.Height),
                    XStringFormats.TopLeft);
                    rowNumber++;
                }

                // Print FOOTER
                if (rowNumber + tmpFooter.Count > rowsPerPage)
                {
                    rowNumber = rowsPerPage - tmpFooter.Count - 1;
                    // ERROR: To many rows are printed.
                    //Console.WriteLine($"ERROR: To many rows are printed at page {pagenumber} of {fileName} file.");
                    //Console.WriteLine("press enter");
                    //Console.ReadLine();
                    throw new ArgumentOutOfRangeException($"ERROR: To many rows are printed at page {pagenumber} of {fileName} file.");
                }
                else
                {
                    rowNumber = rowsPerPage - tmpFooter.Count - 1;
                }
                foreach (var item in tmpFooter)
                {
                    gfx.DrawString($"{item}", font, XBrushes.LightGray,
                    new XRect(textOffsetX, textOffsetY + rowNumber * fontSize, page.Width, page.Height),
                    XStringFormats.TopLeft);
                    rowNumber++;
                }
            } while (queueString.Count > 0);

            // Save the document...
            //string uniqueStamp = $"{DateTime.Now.Year.ToString("D4")}-{DateTime.Now.Month.ToString("D2")}-{DateTime.Now.Day.ToString("D2")}_{DateTime.Now.Hour.ToString("D2")}-{DateTime.Now.Minute.ToString("D2")}-{DateTime.Now.Second.ToString("D2")}-{DateTime.Now.Millisecond.ToString("D3")}";
            var uniqueStamp = $"{DateTime.Now.Date.ToString().Substring(0, 9)}__{DateTime.Now.Hour.ToString("D2")}-{DateTime.Now.Minute.ToString("D2")}-{DateTime.Now.Second.ToString("D2")}-{DateTime.Now.Millisecond.ToString("D3")}";
            string fileNameFull = $"{filePath}\\{fileName}_{reportType}___{uniqueStamp}.pdf";
            fileNameFull = fileNameFull.Replace(" ", "_");
            document.Save(fileNameFull);

            // ...and start a viewer.
            //Process.Start(filename);
            // pdf sharp

            //Console.WriteLine($"created {fileNameFull}");
            //Console.WriteLine();
            return (fileNameFull, true);
        }

        private static string testString()
        {
            return @"Умницата Грета

Имало една готвачка на име Грета. Тя носела обувки с червени токове и като излизала с тях, фръцкала се весело насам-натам и си мислела: „Ама че хубава мома съм!“. А щом се върнела вкъщи, още й било толкова весело на душата, че пийвала глътка вино. А тъй като виното събужда желание за ядене, почвала да опитва най-хубавите ястия и опитвала, докато усетела, че е сита. И си казвала:

— Всяка готвачка трябва да знае вкуса на гозбите си.

Веднъж господарят й рекъл:

— Грета, довечера ще имам гост. Опечи две хубави пилета!

— Ще опека, господарю — отвърнала Грета.

Заклала тя пилетата, попарила ги, оскубала ги, набола ги на шиш и привечер ги сложила над жарта да се опекат. Почнали пилетата да се зачервяват и да омекват, но гостът все още го нямало.

Отишла Грета при господаря и му рекла:

— Ако гостът не дойде, ще трябва да сваля пилетата от жарта. Жалко, че няма да бъдат изядени скоро, докато се още толкова сочни.

Господарят рекъл:

— Ще изтичам да доведа госта.

Излязъл господарят, а Грета сложила настрана шиша с пилетата и си помислила: „Като стои човек дълго край огъня, започва да се поти и ожаднява. Кой ги знае кога ще дойдат! Докато още ги няма, ще сляза в избата да пийна една глътка.“

Изтичала по стълбата в избата, вдигнала една кана и рекла:

— Наздраве, Грета!

И отпила юнашки от каната.

— Вино сладко, гърло гладко; аз не искам, то само влиза — рекла тя след малко и пийнала още веднъж от каната.

Върнала се в кухнята, сложила пак пилетата над жарта, понамазала ги с масло и почнала весело да върти шиша. Запечените пилета ухаели много приятно, но Грета помислила, че може нещо да не им достига и трябва да се опитат. Понатиснала ги с пръст, после го облизала и рекла:

— Чудесни пилета. Срамота и грехота е, че няма да ги ядат веднага.

Изтичала до прозореца да види дали господарят й не се връща с госта, но никой не се мяркал. Върнала се при пилетата и си рекла:

— Едното крилце ще прегори, по-добре ще бъде да го изям.

Отрязала го, изяла го и много й се усладило. Като го изяла, сетила се, че трябва да откъсне и второто крилце, за да не забележи господарят, че нещо не е в ред.

След като изяла двете крилца, отишла пак до прозореца и надникнала навън, но не видяла господаря. „Кой знае дали изобщо ще дойдат! Сигурно са се отбили някъде“ — помислила тя. После си рекла:

— Карай, Грета! Едното вече е наядено; пийни още една глътка вино и изяж цялото пиле. Видиш ли му края, ще се успокоиш. Бива ли такива вкусни неща да се похабят?

Слязла повторно в избата, надигнала каната с виното, а после с най-голямо удоволствие изяла пилето.

Като свършила с едното и господарят все още го нямало, Грета погледнала другото и рекла:

— Където е първото, там трябва да отиде и второто; двете бяха неразделни. Какъвто е късметът на едното, такъв трябва да бъде и на другото. Мисля, че няма да е лошо, ако пийна още една глътка.

Пила и изпратила второто пиле при първото.

Тъкмо си облизвала пръстите, върнал се господарят и викнал:

— Побързай, Грета, гостът ей сега ще дойде!

— Да, господарю, готова съм вече — отвърнала Грета.

Господарят проверил дали масата е добре наредена, взел големия нож, с който щял да разреже пилетата, и отишъл да го наточи.

Скоро гостът дошъл и почукал на пътната врата. Изтичала Грета да види кой е и като зърнала госта, вдигнала пръст до устата си и рекла:

— Тихо! Бягайте веднага оттук! Ако ви види господарят, лошо ще си изпатите. Поканил ви е на вечеря, но си е наумил да ви отреже двете уши. Слушайте само как си точи ножа!

Гостът чул, че се точи нож и хукнал надолу по стълбата колкото му държали краката. Грета пък, без да губи време, запищяла, втурнала се при господаря и викнала:

— Ама че хубостник сте поканили на гости!

— Защо, Грета? Какво говориш?

А тя рекла:

— Тъкмо слагах двете пилета на чинията, за да ги занеса на трапезата, той ги грабна и побягна с тях.

— Това на нищо не прилича! — викнал господарят и му дожаляло за хубавите пилета. — Да беше оставил поне едното, та да имаше и аз какво да ям!

Викнал му да спре, но гостът се престорил, че не чува. Спуснал се тогава след него все още с ножа в ръка и закрещял:

— Дай поне едното! Дай поне едното!

Искал да каже на госта да не отнася двете пилета, а да остави едното за него. Но гостът помислил, че трябва да се прости поне с едното си ухо и бягал така, като че ли му парело под краката, само и само да запази двете си уши.
Край

The C# guide provides many resources about the C# language. This site has many different audiences. Depending on your experience with programming, or with the C# language and .NET, you may wish to explore different sections of this guide.

    For brand-new developers:
        Start with the Introduction to C# tutorials. These tutorials let you explore the C# language interactively in your browser. From there, you can move on to other tutorials. These tutorials show you how to create C# programs from scratch. The tutorials provide a step-by-step process to create programs. They show the language concepts and how to build C# programs on your own. If you prefer reading overview information first, try the tour of the C# language. It explains the concepts of the C# language. After reading this, you'll have a basic understanding of the language, and be ready to try the tutorials, or build something on your own.

    For developers new to C#:
        If you've done development before, but are new to C#, read the tour of the C# language. It covers the basic syntax and structure for the language, and you can use the language tour to contrast C# with other languages you've used. You can also browse the tutorials to try basic C# programs.

    Experienced C# developers:
        If you've used C# before, you should start by reading what's in the latest version of the language. Check out What's new in C# for the new features in the current version.

How the C# guide is organized

There are several sections in the C# Guide. You can read them in order, or jump directly to what interests you the most. Some of the sections are heavily focused on the language. Others provide end-to-end scenarios that demonstrate a few of the types of programs you can create using C# and the .NET Framework.

    Get Started
        This section covers what you need to install for a C# development environment on your preferred platform. The different topics under this section explain how to create your first C# program in different supported environments.

    Introduction to C# tutorials:
        Introduction to C# tutorials presents interactive tutorials for brand-new developers to explore and learn the C# language in the browser using a Read-Eval-Print Loop (REPL) interface. After you finish the interactive lessons, you can improve your coding skills by practicing the same lessons on your machine.

    Tutorials
        This section provides a variety of end-to-end scenarios, including descriptions and code. It shows why certain idioms are preferred, what C# features work best in different situations, and reference implementations for common tasks. If you learn best by seeing code, start in this section. You can also download all the code and experiment in your own environment.

    Tour of C#
        This section provides an overview of the language. It covers the elements that make up C# programs and the capabilities of the language. It shows small samples of all the syntax elements of C# and discussions of the major C# language topics.

    What's new in C#
        Provides overviews of new features added in the latest language releases and of the history of the C# language.

    C# Programming Guide
        Provides information and practical examples about how to use C# language constructs.

    Walkthroughs
        Provides links to programming walkthroughs that use C# and a brief description of each walkthrough.

    Language Reference
        This section contains the reference material on the C# language. This material helps you understand the syntax and semantics of C#. It also includes reference material on types, operators, attributes, preprocessor directives, compiler switches, compiler errors, and compiler warnings.

    C# Language Specification
        Links to the latest versions of the C# language specification.";
        }

        public string Write(string message)
        {
            int textOffsetX = 50;
            int textOffsetY = 30;
            int fontSize = 10;
            string reportType = "offer";
            string fileLocationPath = "C:\\StoreManagementReports";
            string fileNameBeginning = "StoreManagementReport";
            var result = PDFReportWriter.WriteReportToUniquePDF(
                message,
                //reportType,
                fileLocationPath,
                fileNameBeginning,
                fontSize,
                textOffsetX,
                textOffsetY);
            if (result.isCreated)
            {
                return $"PDF report created at {result.fileName}";
            }
            else
            {
                return "ERROR: PDF report not created";
            }
        }
    }
}


//var reportType = "offer";
//var textOffsetX = 50;
//var textOffsetY = 30;
//var fileLocationPath = "C:\\StoreManagementReports";
//PDFReportWriter.WriteToUniquePDFFile(PDFReportWriter.testString(), reportType, fileLocationPath, "StoreManagementReport", textOffsetX, textOffsetY);
